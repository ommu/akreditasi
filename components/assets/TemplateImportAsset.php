<?php
namespace ommu\akreditasi\components\assets;

class TemplateImportAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@ommu/akreditasi/components/import';

	public $publishOptions = [
		'forceCopy' => YII_DEBUG? true: false,
	];

}