<style type="text/css">
	html, body, div, span, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, code,
	del, dfn, em, img, q, dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	input, button, select, textarea,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, details, figcaption, figure, footer, header,
	hgroup, nav, section {
		color: #111;
		font-size: 13px;
		line-height: 17px;
		font-weight: 400;
	}
    h1 {
        font-weight: bold;
        text-align: center;
        font-size: 16px;
    }
	table {width: 100%; border-collapse: collapse; border-spacing: 0;}
	table.profile th,
	table.profile td {
		padding: 10px 10px;
	}
	table.profile th {
		border: 1px solid #aaa;
		font-weight: bold;
		text-align: left;
	}
	table.profile td {
		border: 1px solid #ccc;
	}
	table.profile td.center {
		text-align: center;
	}
    ol {
        margin: 0;
        padding: 0;
    }
    ol li {
        padding-bottom: 7px;
    }
	div.copyright,
	div.copyright * {
		font-size: 12px;
		line-height: 15px;
		color: #bbb;
	}
	div.copyright {
		position: absolute;
		left: 0;
		bottom: 25mm;
		width: 100%;
		padding: 0 0 20px 0;
		text-align: center;
	}
	div.copyright a {
		text-decoration: none;
		font-weight: bold;
	}
</style>

<page backtop="15mm" backbottom="20mm" backleft="25mm" backright="20mm" style="font-size: 12pt">
<div style="height: 99%;">
    <h1 style="text-align: center;"><?php echo strtoupper('Akreditasi '.$model->library->category->category_name);?></h1>

    <br/>

	<table class="profile" style="width: 100%;">
		<tr>
			<th style="vertical-align: top; text-align: center;" colspan="2">PENGAJUAN <?php echo strtoupper('Akreditasi '.$model->library->category->category_name);?></th>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Hari/tanggal</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $generatedDateHead;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top;; text-align: center;" colspan="2">IDENTITAS PERPUSTAKAAN</th>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nama Perpustakaan</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $model->library->library_name;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nomor NPP</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $model->library->npp;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nilai Akreditasi Sebelumnya</th>
			<td style="vertical-align: top; width: 70%;"><?php echo strtoupper($model->library->accreditation);?></td>
		</tr>
		<!-- <tr>
			<th style="vertical-align: top; width: 30%;">Tipologi Perpustakaan</th>
			<td style="vertical-align: top; width: 70%;"><?php // echo $model->library->accreditation;?></td>
		</tr> -->
		<tr>
			<th style="vertical-align: top; width: 30%;">Alamat</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $place;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Kelurahan</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $village;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Kecamatan</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $district;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Kabupaten/kota</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $city;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Provinsi</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $province;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Email</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $email;?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">No. Telp/Fax/HP</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $phoneFax;?></td>
		</tr>
        <?php if ($headDepartment) {?>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nama <?php echo ucwords($headDepartment);?></th>
			<td style="vertical-align: top; width: 70%;"><?php echo $model->librarian['headDepartment'];?></td>
		</tr>
        <?php }?>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nama <?php echo ucwords($headLibrary);?></th>
			<td style="vertical-align: top; width: 70%;"><?php echo $model->librarian['headLibrary'];?></td>
		</tr>
		<tr>
			<th style="vertical-align: top; width: 30%;">Nama Tenaga Perpustakaan</th>
			<td style="vertical-align: top; width: 70%;"><?php echo $librarian;?></td>
		</tr>
	</table>

    <br/>

    <table style="width: 100%;">
        <tr>
            <td style="vertical-align: top; width: 100%; text-align: right;" colspan="3"><?php echo $city;?>, <?php echo $generatedDateSignature;?></td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 35%; text-align: center;"><?php echo $headDepartment ? 'Mengetahui :<br/>'.$headDepartment.',' : '';?></td>
            <td style="vertical-align: top; width: 30%;">&nbsp;&nbsp;</td>
            <td style="vertical-align: bottom; width: 35%; text-align: center;"><?php echo $headLibrary;?></td>
        </tr>
        <tr>
            <td style="vertical-align: bottom; width: 35%; text-align: center;"><br/><br/><br/><br/><br/><?php echo $model->librarian['headDepartment'];?></td>
            <td style="vertical-align: top; width: 30%;">&nbsp;&nbsp;</td>
            <td style="vertical-align: bottom; width: 35%; text-align: center;"><br/><br/><br/><br/><br/><?php echo $model->librarian['headLibrary'];?></td>
        </tr>
    </table>
</div>
</page>