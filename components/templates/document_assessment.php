<style type="text/css">
	html, body, div, span, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, code,
	del, dfn, em, img, q, dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	input, button, select, textarea,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, details, figcaption, figure, footer, header,
	hgroup, nav, section {
		color: #111;
		font-size: 13px;
		line-height: 17px;
		font-weight: 400;
	}
    h1 {
        font-weight: bold;
        text-align: center;
        font-size: 15px;
        line-height: 20px;
        margin-bottom: 20px;
    }
	table {width: 100%; border-collapse: collapse; border-spacing: 0;}
	table.profile th,
	table.profile td {
		padding: 10px 10px;
	}
	table.profile th {
		border: 1px solid #aaa;
		font-weight: bold;
		text-align: left;
	}
	table.profile td {
		border: 1px solid #ccc;
	}
	table.profile td.center {
		text-align: center;
	}
	table.profile td.center {
		text-align: center;
	}
	div.copyright,
	div.copyright * {
		font-size: 12px;
		line-height: 15px;
		color: #bbb;
	}
	div.copyright {
		position: absolute;
		left: 0;
		bottom: 25mm;
		width: 100%;
		padding: 0 0 20px 0;
		text-align: center;
	}
	div.copyright a {
		text-decoration: none;
		font-weight: bold;
	}
</style>

<page backtop="15mm" backbottom="20mm" backleft="20mm" backright="20mm" style="font-size: 12pt">
<div style="height: 99%;">
    <h1 style="text-align: center;"><?php echo strtoupper('Lembar Kerja Penilaian Mandiri<br/>Akreditasi '.$model->library->category->category_name);?></h1>

    <table style="width: 100%;">
        <tr>
            <td style="vertical-align: top; width: 25%;">Nama Perpustakaan</td>
            <td style="vertical-align: top; width: 2%;">:&nbsp;&nbsp;</td>
            <td style="vertical-align: top; width: 65%;"><?php echo $model->library->library_name;?> / <?php echo $model->library->city->city_name;?></td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 25%;">Nama Pustakawan</td>
            <td style="vertical-align: top; width: 2%;">:&nbsp;&nbsp;</td>
            <td style="vertical-align: top; width: 65%;"><?php echo $model->user->displayname;?></td>
        </tr>
    </table>

    <br/>

    <b>Petunjuk:</b>
    <br/>
    Isilah Lembar Kerja Instrumen Akreditasi sesuai dengan kondisi perpustakaan saudara dengan mencantumkan huruf A, B, C, D atau E pada kolom Skor Huruf. Kemudian pada kolom Skor Angka isilah dengan angka 1 sampai dengan angka 5, dengan ketentuan sebagai berikut, bila Skor A = 5; Skor B = 4; Skor C = 3; Skor D = 2; dan bila Skor E = 1;

    <br/>
    <br/>

    <?php echo $assessmentHtml;?>

</div>
</page>