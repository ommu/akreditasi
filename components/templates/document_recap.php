<style type="text/css">
	html, body, div, span, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, code,
	del, dfn, em, img, q, dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	input, button, select, textarea,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, details, figcaption, figure, footer, header,
	hgroup, nav, section {
		color: #111;
		font-size: 13px;
		line-height: 17px;
		font-weight: 400;
	}
    h1 {
        font-weight: bold;
        text-align: center;
        font-size: 15px;
        line-height: 20px;
        margin-bottom: 20px;
    }
	table {width: 100%; border-collapse: collapse; border-spacing: 0;}
	table.profile th,
	table.profile td {
		padding: 10px 10px;
	}
	table.profile th {
		border: 1px solid #aaa;
		font-weight: bold;
		text-align: left;
	}
	table.profile td {
		border: 1px solid #ccc;
	}
	table.profile td.center {
		text-align: center;
	}
    ol {
        margin: 0;
        padding: 0;
    }
    ol li {
        padding-bottom: 7px;
    }
	div.copyright,
	div.copyright * {
		font-size: 12px;
		line-height: 15px;
		color: #bbb;
	}
	div.copyright {
		position: absolute;
		left: 0;
		bottom: 25mm;
		width: 100%;
		padding: 0 0 20px 0;
		text-align: center;
	}
	div.copyright a {
		text-decoration: none;
		font-weight: bold;
	}
</style>

<page backtop="15mm" backbottom="20mm" backleft="20mm" backright="20mm" style="font-size: 12pt">
<div style="height: 99%;">
    <h1 style="text-align: center;"><?php echo strtoupper('Rekap Lembar Kerja Penilaian Mandiri<br/>Akreditasi '.$model->library->category->category_name);?></h1>

    <table style="width: 100%;">
        <tr>
            <td style="vertical-align: top; width: 25%;">Nama Perpustakaan</td>
            <td style="vertical-align: top; width: 2%;">:&nbsp;&nbsp;</td>
            <td style="vertical-align: top; width: 65%;"><?php echo $model->library->library_name;?> / <?php echo $model->library->city->city_name;?></td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 25%;">Nama Pustakawan</td>
            <td style="vertical-align: top; width: 2%;">:&nbsp;&nbsp;</td>
            <td style="vertical-align: top; width: 65%;"><?php echo $model->user->displayname;?></td>
        </tr>
    </table>

    <br/>

    <table class="profile" style="width: 100%;">
        <tr>
            <th style="vertical-align: middle; text-align: center;">No</th>
            <th style="vertical-align: middle; text-align: center;">Komponen</th>
            <th style="vertical-align: middle; text-align: center;">Jumlah<br/>Skor</th>
            <th style="vertical-align: middle; text-align: center;">Nilai<br/>Maksimal</th>
            <th style="vertical-align: middle; text-align: center;">Bobot</th>
            <th style="vertical-align: middle; text-align: center;">Nilai Akhir</th>
        </tr>
        <?php $histories = $model->getHistories(true);
        if ($histories) {
            $i == 0;
            foreach ($model->histories as $step) {
                $i++;
                $maxScore = $step->component->getIndicators('count', 1, true) * 5;
                $bobot = $step->component->bobot / 100;
                $stepScore = number_format(($step->step_component_score / $maxScore) * $bobot, 2); ?>
                <tr>
                    <td style="vertical-align: top;"><?php echo $i;?></td>
                    <td style="vertical-align: top;"><?php echo $step->component->component_name;?></td>
                    <td style="vertical-align: top; text-align: center;"><?php echo $step->step_component_score;?></td>
                    <td style="vertical-align: top; text-align: center;"><?php echo $maxScore;?></td>
                    <td style="vertical-align: top; text-align: center;"><?php echo $step->component->bobot;?></td>
                    <td style="vertical-align: top; text-align: center;"><?php echo $stepScore * 100;?></td>
                </tr>
        <?php }
        }?>
        <tr>
            <td style="vertical-align: top;"></td>
            <td style="vertical-align: top;" colspan="3">Total</td>
            <td style="vertical-align: top; text-align: center;">100</td>
            <td style="vertical-align: top; text-align: center;"><?php echo $model->simulation_score;?></td>
        </tr>
    </table>

    <br/><br/>

    <b>Keterangan:</b>

    <ol style="padding: 0; margin: 0;">
        <li>Jumlah Skor masing-masing komponen diperoleh dari penjumlahan nilai angka dari butir pernyataan yang ada di masing-masing komponen.</li>
        <li>Nilai Maksimal masing-masing komponen diperoleh dari jumlah butir pernyataan masing-masing komponen dikalikan angka 5.</li>
        <li>Nilai Akhir masing-masing komponen diperoleh dari jumlah skor dibagi Nilai Maksimal kemudian dikalikan dengan bobot nilai masing-masing komponen.</li>
        <li>Total Nilai Akhir diperoleh dari penjumlahan hasil Nilai Akhir masing-masing komponen.</li>
    </ol>
</div>
</page>