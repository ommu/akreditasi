<?php
/**
 * m201112_100802_akreditasi_module_insert_menu
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 12 November 2020, 14:14 WIB
 * @modified date 12 November 2020, 17:12 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use Yii;
use mdm\admin\components\Configs;
use app\models\Menu;

class m201112_100802_akreditasi_module_insert_menu extends \yii\db\Migration
{
	public function up()
	{
        $menuTable = Configs::instance()->menuTable;
		$tableName = Yii::$app->db->tablePrefix . $menuTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['name', 'module', 'icon', 'parent', 'route', 'order', 'data'], [
				['SAMBA', 'akreditasi', 'fa-certificate', null, '/#', null, null],
			]);

			$this->batchInsert($tableName, ['name', 'module', 'icon', 'parent', 'route', 'order', 'data'], [
				['Simulations', 'akreditasi', null, Menu::getParentId('SAMBA#akreditasi'), '/akreditasi/admin/index', '1', null],
				['Library', 'akreditasi', null, Menu::getParentId('SAMBA#akreditasi'), '/akreditasi/library/admin/index', '2', null],
				['Instrument', 'akreditasi', null, Menu::getParentId('SAMBA#akreditasi'), '/akreditasi/instrument/component/index', '3', null],
				['Settings', 'akreditasi', null, Menu::getParentId('SAMBA#akreditasi'), '/akreditasi/setting/admin/index', '4', null],
			]);
		}
	}
}
