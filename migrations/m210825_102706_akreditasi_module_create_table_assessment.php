<?php
/**
 * m210825_102706_akreditasi_module_create_table_assessment
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 25 August 2021, 10:27 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use Yii;
use yii\db\Schema;

class m210825_102706_akreditasi_module_create_table_assessment extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_assessment';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'simulation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'indicator_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'choice' => Schema::TYPE_CHAR . '(1) NOT NULL',
				'creation_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger\'',
				'creation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'PRIMARY KEY ([[id]])',
				'CONSTRAINT ommu_akreditasi_assessment_ibfk_1 FOREIGN KEY ([[simulation_id]]) REFERENCES ommu_akreditasi_simulation ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
				'CONSTRAINT ommu_akreditasi_assessment_ibfk_2 FOREIGN KEY ([[indicator_id]]) REFERENCES ommu_akreditasi_indicator ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
			], $tableOptions);

			$this->createIndex(
				'simulation_indicator',
				$tableName,
				['simulation_id', 'indicator_id']
			);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_assessment';
		$this->dropTable($tableName);
	}
}
