<?php
/**
 * m210825_102637_akreditasi_module_create_table_simulation_history
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 25 August 2021, 10:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use Yii;
use yii\db\Schema;

class m210825_102637_akreditasi_module_create_table_simulation_history extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_simulation_history';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'simulation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',
				'user_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',
				'step_component_id' => Schema::TYPE_SMALLINT . '(5) UNSIGNED NOT NULL',
				'step_component_date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
				'step_component_score' => Schema::TYPE_SMALLINT . '(4) NOT NULL',
				'PRIMARY KEY ([[id]])',
				'CONSTRAINT ommu_akreditasi_simulation_history_ibfk_1 FOREIGN KEY ([[simulation_id]]) REFERENCES ommu_akreditasi_simulation ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
			], $tableOptions);

			$this->createIndex(
				'simulation_userId_stepComponent',
				$tableName,
				['simulation_id', 'user_id', 'step_component_id']
			);

			$this->createIndex(
				'simulation_stepComponent',
				$tableName,
				['simulation_id', 'step_component_id']
			);

			$this->createIndex(
				'user_id',
				$tableName,
				['user_id']
			);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_simulation_history';
		$this->dropTable($tableName);
	}
}
