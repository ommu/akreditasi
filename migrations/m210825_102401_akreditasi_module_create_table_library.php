<?php
/**
 * m210825_102401_akreditasi_module_create_table_library
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 25 August 2021, 10:24 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use Yii;
use yii\db\Schema;

class m210825_102401_akreditasi_module_create_table_library extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_library';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'publish' => Schema::TYPE_TINYINT . '(1) NOT NULL DEFAULT \'1\'',
				'cat_id' => Schema::TYPE_SMALLINT . '(5) UNSIGNED',
				'typology' => Schema::TYPE_STRING,
				'library_name' => Schema::TYPE_TEXT . ' NOT NULL',
				'npp' => Schema::TYPE_STRING . '(32) NOT NULL',
				'library_address' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'library_contact' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'library_city' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'accreditation' => Schema::TYPE_CHAR . '(1) NOT NULL',
				'creation_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger\'',
				'creation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'modified_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger\'',
				'modified_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'updated_date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT \'0000-00-00 00:00:00\' COMMENT \'trigger\'',
				'PRIMARY KEY ([[id]])',
				'CONSTRAINT ommu_akreditasi_library_ibfk_1 FOREIGN KEY ([[cat_id]]) REFERENCES ommu_akreditasi_category ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
			], $tableOptions);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_akreditasi_library';
		$this->dropTable($tableName);
	}
}
