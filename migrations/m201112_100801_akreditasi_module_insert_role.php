<?php
/**
 * m201112_100801_akreditasi_module_insert_role
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 12 November 2020, 14:14 WIB
 * @modified date 12 November 2020, 17:11 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use Yii;
use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

class m201112_100801_akreditasi_module_insert_role extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }

        return $authManager;
    }

	public function up()
	{
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;
        $schema = $this->db->getSchema()->defaultSchema;

		$tableName = Yii::$app->db->tablePrefix . $authManager->itemTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['name', 'type', 'data', 'created_at'], [
				['akreditasiModLevelAdmin', '2', '', time()],
				['akreditasiModLevelModerator', '2', '', time()],
                ['/akreditasi/setting/admin/index', '2', '', time()],
                ['/akreditasi/setting/admin/update', '2', '', time()],
                ['/akreditasi/setting/admin/delete', '2', '', time()],
                ['/akreditasi/setting/category/*', '2', '', time()],
                ['/akreditasi/setting/category/index', '2', '', time()],
                ['/akreditasi/instrument/component/*', '2', '', time()],
                ['/akreditasi/instrument/component/index', '2', '', time()],
                ['/akreditasi/instrument/indicator/*', '2', '', time()],
                ['/akreditasi/instrument/indicator/index', '2', '', time()],
                ['/akreditasi/admin/*', '2', '', time()],
                ['/akreditasi/admin/index', '2', '', time()],
                ['/akreditasi/library/admin/*', '2', '', time()],
                ['/akreditasi/library/admin/index', '2', '', time()],
                ['/akreditasi/library/officer/*', '2', '', time()],
                ['/akreditasi/o/step/*', '2', '', time()],
                ['/akreditasi/o/assessment/*', '2', '', time()],
			]);
		}

		$tableName = Yii::$app->db->tablePrefix . $authManager->itemChildTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['parent', 'child'], [
				['userAdmin', 'akreditasiModLevelAdmin'],
				['userModerator', 'akreditasiModLevelModerator'],
				['akreditasiModLevelAdmin', 'akreditasiModLevelModerator'],
				['akreditasiModLevelAdmin', '/akreditasi/setting/admin/update'],
				['akreditasiModLevelAdmin', '/akreditasi/setting/admin/delete'],
				['akreditasiModLevelAdmin', '/akreditasi/setting/category/*'],
				['akreditasiModLevelModerator', '/akreditasi/setting/admin/index'],
				['akreditasiModLevelModerator', '/akreditasi/instrument/component/*'],
				['akreditasiModLevelModerator', '/akreditasi/instrument/indicator/*'],
				['akreditasiModLevelModerator', '/akreditasi/admin/*'],
				['akreditasiModLevelModerator', '/akreditasi/library/admin/*'],
				['akreditasiModLevelModerator', '/akreditasi/library/officer/*'],
				['akreditasiModLevelModerator', '/akreditasi/o/step/*'],
				['akreditasiModLevelModerator', '/akreditasi/o/assessment/*'],
			]);
		}
	}
}
