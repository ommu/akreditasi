<?php
/**
 * AkreditasiSimulation
 *
 * AkreditasiSimulation represents the model behind the search form about `ommu\akreditasi\models\AkreditasiSimulation`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\akreditasi\models\AkreditasiSimulation as AkreditasiSimulationModel;

class AkreditasiSimulation extends AkreditasiSimulationModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'publish', 'library_id', 'user_id', 'simulation_score', 'modified_id', 'history', 'completed', 'document'], 'integer'],
            [['simulation_start', 'simulation_end', 'simulation_document', 'generated_date', 'creation_date', 'modified_date', 'updated_date', 
                'libraryName', 'userDisplayname', 'modifiedDisplayname', 'accreditation'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = AkreditasiSimulationModel::find()->alias('t');
        } else {
            $query = AkreditasiSimulationModel::find()->alias('t')
                ->select($column);
        }
		$query->joinWith([
			// 'library library', 
			// 'user user', 
			// 'modified modified'
		]);
        if ((isset($params['sort']) && in_array($params['sort'], ['libraryName', '-libraryName'])) || (isset($params['libraryName']) && $params['libraryName'] != '')) {
            $query->joinWith(['library library']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['userDisplayname', '-userDisplayname'])) || (isset($params['userDisplayname']) && $params['userDisplayname'] != '')) {
            $query->joinWith(['user user']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['modifiedDisplayname', '-modifiedDisplayname'])) || (isset($params['modifiedDisplayname']) && $params['modifiedDisplayname'] != '')) {
            $query->joinWith(['modified modified']);
        }
        if (isset($params['history']) && $params['history'] != '') {
            $query->joinWith(['histories histories']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['libraryName'] = [
			'asc' => ['library.library_name' => SORT_ASC],
			'desc' => ['library.library_name' => SORT_DESC],
		];
		$attributes['userDisplayname'] = [
			'asc' => ['user.displayname' => SORT_ASC],
			'desc' => ['user.displayname' => SORT_DESC],
		];
		$attributes['modifiedDisplayname'] = [
			'asc' => ['modified.displayname' => SORT_ASC],
			'desc' => ['modified.displayname' => SORT_DESC],
		];
		$attributes['accreditation'] = [
			'asc' => ['t.simulation_score' => SORT_ASC],
			'desc' => ['t.simulation_score' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
		$query->andFilterWhere([
			't.id' => $this->id,
			't.library_id' => isset($params['library']) ? $params['library'] : $this->library_id,
			't.user_id' => isset($params['user']) ? $params['user'] : $this->user_id,
			'cast(t.simulation_start as date)' => $this->simulation_start,
			'cast(t.simulation_end as date)' => $this->simulation_end,
			't.simulation_score' => $this->simulation_score,
			'cast(t.generated_date as date)' => $this->generated_date,
			'cast(t.creation_date as date)' => $this->creation_date,
			'cast(t.modified_date as date)' => $this->modified_date,
			't.modified_id' => isset($params['modified']) ? $params['modified'] : $this->modified_id,
			'cast(t.updated_date as date)' => $this->updated_date,
		]);

		if (isset($params['trash'])) {
            $query->andFilterWhere(['NOT IN', 't.publish', [0,1]]);
        } else {
            if (!isset($params['publish']) || (isset($params['publish']) && $params['publish'] == '')) {
                $query->andFilterWhere(['IN', 't.publish', [0,1]]);
            } else {
                $query->andFilterWhere(['t.publish' => $this->publish]);
            }
        }

		if (isset($params['history']) && $params['history'] != '') {
            if ($this->history == 1) {
                $query->andWhere(['is not', 'histories.id', null]);
            } else if ($this->history == 0) {
                $query->andWhere(['is', 'histories.id', null]);
            }
        }

		if (isset($params['completed']) && $params['completed'] != '') {
            if ($this->completed == 1) {
                $query->andWhere(['<>', 'cast(t.simulation_end as date)', '0000-00-00']);
            } else if ($this->completed == 0) {
                $query->andWhere(['cast(t.simulation_end as date)' => '0000-00-00']);
            }
        }

		if (isset($params['accreditation']) && $params['accreditation'] != '') {
            $query->andWhere(['<>', 'cast(t.simulation_end as date)', '0000-00-00']);
            if ($this->accreditation == '0') {
                $query->andWhere(['<>', 't.simulation_score', 0])
                    ->andWhere(['<', 't.simulation_score', 60]);
            } else if ($this->accreditation == 'a') {
                $query->andWhere(['>=', 't.simulation_score', 91])
                    ->andWhere(['<=', 't.simulation_score', 100]);
            } else if ($this->accreditation == 'b') {
                $query->andWhere(['>=', 't.simulation_score', 76])
                    ->andWhere(['<=', 't.simulation_score', 90]);
            } else if ($this->accreditation == 'c') {
                $query->andWhere(['>=', 't.simulation_score', 60])
                    ->andWhere(['<=', 't.simulation_score', 75]);
            }
        }

		if (isset($params['document']) && $params['document'] != '') {
            if ($this->document == 1) {
                $query->andWhere(['<>', 'cast(t.generated_date as date)', '0000-00-00']);
            } else if ($this->document == 0) {
                $query->andWhere(['cast(t.generated_date as date)' => '0000-00-00']);
            }
        }

        $query->andFilterWhere(['like', 'library.library_name', $this->libraryName])
            ->andFilterWhere(['like', 'user.displayname', $this->userDisplayname])
            ->andFilterWhere(['like', 't.simulation_document', $this->simulation_document])
			->andFilterWhere(['like', 'modified.displayname', $this->modifiedDisplayname]);

		return $dataProvider;
	}
}
