<?php
/**
 * AkreditasiLibrary
 *
 * AkreditasiLibrary represents the model behind the search form about `ommu\akreditasi\models\AkreditasiLibrary`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\akreditasi\models\AkreditasiLibrary as AkreditasiLibraryModel;
use ommu\akreditasi\models\AkreditasiSimulation;

class AkreditasiLibrary extends AkreditasiLibraryModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'publish', 'cat_id', 'library_city', 'creation_id', 'modified_id', 'officer', 'simulation'], 'integer'],
            [['library_name', 'npp', 'library_address', 'library_contact', 'accreditation', 'creation_date', 'modified_date', 'updated_date', 
                'categoryName', 'cityName', 'creationDisplayname', 'modifiedDisplayname'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = AkreditasiLibraryModel::find()->alias('t');
        } else {
            $query = AkreditasiLibraryModel::find()->alias('t')
                ->select($column);
        }
		$query->joinWith([
			// 'category category', 
			// 'creation creation', 
			// 'modified modified'
		]);

        if ((isset($params['sort']) && in_array($params['sort'], ['cat_id', '-cat_id', 'categoryName', '-categoryName'])) || (isset($params['categoryName']) && $params['categoryName'] != '')) {
            $query->joinWith(['category category']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['cityName', '-cityName'])) || (isset($params['cityName']) && $params['cityName'] != '')) {
            $query->joinWith(['city city']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['creationDisplayname', '-creationDisplayname'])) || (isset($params['creationDisplayname']) && $params['creationDisplayname'] != '')) {
            $query->joinWith(['creation creation']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['modifiedDisplayname', '-modifiedDisplayname'])) || (isset($params['modifiedDisplayname']) && $params['modifiedDisplayname'] != '')) {
            $query->joinWith(['modified modified']);
        }
        if (isset($params['officer']) && $params['officer'] != '') {
            $query->joinWith(['officers officers']);
        }
        if (isset($params['simulation']) && $params['simulation'] != '') {
            $query->joinWith(['simulations simulations']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['cat_id'] = [
			'asc' => ['category.category_name' => SORT_ASC],
			'desc' => ['category.category_name' => SORT_DESC],
		];
		$attributes['cityName'] = [
			'asc' => ['city.city_name' => SORT_ASC],
			'desc' => ['city.city_name' => SORT_DESC],
		];
		$attributes['categoryName'] = [
			'asc' => ['category.category_name' => SORT_ASC],
			'desc' => ['category.category_name' => SORT_DESC],
		];
		$attributes['creationDisplayname'] = [
			'asc' => ['creation.displayname' => SORT_ASC],
			'desc' => ['creation.displayname' => SORT_DESC],
		];
		$attributes['modifiedDisplayname'] = [
			'asc' => ['modified.displayname' => SORT_ASC],
			'desc' => ['modified.displayname' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
		$query->andFilterWhere([
			't.id' => $this->id,
			't.cat_id' => isset($params['category']) ? $params['category'] : $this->cat_id,
			't.library_city' => isset($params['city']) ? $params['city'] : $this->library_city,
			'cast(t.creation_date as date)' => $this->creation_date,
			't.creation_id' => isset($params['creation']) ? $params['creation'] : $this->creation_id,
			'cast(t.modified_date as date)' => $this->modified_date,
			't.modified_id' => isset($params['modified']) ? $params['modified'] : $this->modified_id,
			'cast(t.updated_date as date)' => $this->updated_date,
		]);

        if (isset($params['trash'])) {
            $query->andFilterWhere(['NOT IN', 't.publish', [0,1]]);
        } else {
            if (!isset($params['publish']) || (isset($params['publish']) && $params['publish'] == '')) {
                $query->andFilterWhere(['IN', 't.publish', [0,1]]);
            } else {
                $query->andFilterWhere(['t.publish' => $this->publish]);
            }
        }

        if (isset($params['officer']) && $params['officer'] != '') {
            if ($this->officer == 1) {
                $query->andWhere(['is not', 'officers.id', null]);
            } else if ($this->officer == 0) {
                $query->andWhere(['is', 'officers.id', null]);
            }
        }

        if (isset($params['simulation']) && $params['simulation'] != '') {
            if ($this->simulation == 1) {
                $query->andWhere(['is not', 'simulations.id', null]);
            } else if ($this->simulation == 0) {
                $query->andWhere(['is', 'simulations.id', null]);
            }
        }

        if (isset($params['accreditation']) && $params['accreditation'] != '') {
            $accreditation = AkreditasiSimulation::getAccreditations();
            if (array_key_exists($this->accreditation, $accreditation)) {
                $query->andWhere(['t.accreditation' => $this->accreditation]);
            } else if ($this->accreditation == '-') {
                $query->andWhere(['t.accreditation' => '']);
            }
        }

		$query->andFilterWhere(['like', 't.library_name', $this->library_name])
            ->andFilterWhere(['like', 't.npp', $this->npp])
			->andFilterWhere(['like', 't.library_address', $this->library_address])
			->andFilterWhere(['like', 't.library_contact', $this->library_contact])
			->andFilterWhere(['like', 'category.category_name', $this->categoryName])
			->andFilterWhere(['like', 'city.city_name', $this->cityName])
			->andFilterWhere(['like', 'creation.displayname', $this->creationDisplayname])
			->andFilterWhere(['like', 'modified.displayname', $this->modifiedDisplayname]);

		return $dataProvider;
	}
}
