<?php
/**
 * AkreditasiSimulationHistory
 *
 * AkreditasiSimulationHistory represents the model behind the search form about `ommu\akreditasi\models\AkreditasiSimulationHistory`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 6 January 2021, 18:55 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\akreditasi\models\AkreditasiSimulationHistory as AkreditasiSimulationHistoryModel;

class AkreditasiSimulationHistory extends AkreditasiSimulationHistoryModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'simulation_id', 'user_id', 'step_component_id', 'step_component_score'], 'integer'],
			[['step_component_date', 'simulations', 'userDisplayname'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = AkreditasiSimulationHistoryModel::find()->alias('t');
        } else {
            $query = AkreditasiSimulationHistoryModel::find()->alias('t')
                ->select($column);
        }
		$query->joinWith([
			// 'simulation simulation', 
			// 'simulation.library library', 
			// 'user user'
		]);
        if ((isset($params['sort']) && in_array($params['sort'], ['simulations', '-simulations'])) || (isset($params['simulations']) && $params['simulations'] != '')) {
            $query->joinWith([
                'simulation simulation', 
                'simulation.library library', 
            ]);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['userDisplayname', '-userDisplayname'])) || (isset($params['userDisplayname']) && $params['userDisplayname'] != '')) {
            $query->joinWith(['user user']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['simulations'] = [
			'asc' => ['simulation.simulation_start' => SORT_ASC],
			'desc' => ['simulation.simulation_start' => SORT_DESC],
		];
		$attributes['userDisplayname'] = [
			'asc' => ['user.displayname' => SORT_ASC],
			'desc' => ['user.displayname' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
        $query->andFilterWhere([
			't.id' => $this->id,
			't.simulation_id' => isset($params['simulation']) ? $params['simulation'] : $this->simulation_id,
			't.user_id' => isset($params['user']) ? $params['user'] : $this->user_id,
			't.step_component_id' => $this->step_component_id,
			'cast(t.step_component_date as date)' => $this->step_component_date,
			't.step_component_score' => $this->step_component_score,
		]);

		$query->andFilterWhere(['like', 'library.library_name', $this->simulations])
            ->andFilterWhere(['like', 'user.displayname', $this->userDisplayname]);

		return $dataProvider;
	}
}
