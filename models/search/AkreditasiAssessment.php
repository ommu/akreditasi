<?php
/**
 * AkreditasiAssessment
 *
 * AkreditasiAssessment represents the model behind the search form about `ommu\akreditasi\models\AkreditasiAssessment`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:59 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\akreditasi\models\AkreditasiAssessment as AkreditasiAssessmentModel;

class AkreditasiAssessment extends AkreditasiAssessmentModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'simulation_id', 'indicator_id', 'creation_id'], 'integer'],
			[['choice', 'creation_date', 'simulations', 'indicatorQuestionComponent', 'creationDisplayname'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = AkreditasiAssessmentModel::find()->alias('t');
        } else {
            $query = AkreditasiAssessmentModel::find()->alias('t')
                ->select($column);
        }
		$query->joinWith([
			// 'simulation simulation', 
			// 'simulation.library library', 
			// 'simulation.user user', 
			// 'indicator indicator', 
			// 'indicator.component component', 
			// 'creation creation'
		]);
        if ((isset($params['sort']) && in_array($params['sort'], ['simulations', '-simulations'])) || (isset($params['simulations']) && $params['simulations'] != '')) {
            $query->joinWith([
                'simulation simulation', 
                'simulation.library library', 
                'simulation.user user', 
            ]);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['indicatorQuestionComponent', '-indicatorQuestionComponent'])) || (isset($params['indicatorQuestionComponent']) && $params['indicatorQuestionComponent'] != '')) {
            $query->joinWith([
                'indicator indicator', 
                'indicator.component component', 
            ]);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['creationDisplayname', '-creationDisplayname'])) || (isset($params['creationDisplayname']) && $params['creationDisplayname'] != '')) {
            $query->joinWith(['creation creation']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['simulations'] = [
			'asc' => ['simulation.simulation_start' => SORT_ASC],
			'desc' => ['simulation.simulation_start' => SORT_DESC],
		];
		$attributes['indicatorQuestionComponent'] = [
			'asc' => ['indicator.question' => SORT_ASC],
			'desc' => ['indicator.question' => SORT_DESC],
		];
		$attributes['creationDisplayname'] = [
			'asc' => ['creation.displayname' => SORT_ASC],
			'desc' => ['creation.displayname' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
		$query->andFilterWhere([
			't.id' => $this->id,
			't.simulation_id' => isset($params['simulation']) ? $params['simulation'] : $this->simulation_id,
			't.indicator_id' => isset($params['indicator']) ? $params['indicator'] : $this->indicator_id,
			'cast(t.creation_date as date)' => $this->creation_date,
			't.creation_id' => isset($params['creation']) ? $params['creation'] : $this->creation_id,
		]);

		$query->andFilterWhere(['like', 't.choice', $this->choice])
			->andFilterWhere(['or',
                ['like', 'library.library_name', $this->simulations],
                ['like', 'user.displayname', $this->simulations],
            ])
			->andFilterWhere(['or',
                ['like', 'indicator.question', $this->indicatorQuestionComponent],
                ['like', 'component.component_name', $this->indicatorQuestionComponent],
            ])
			->andFilterWhere(['like', 'creation.displayname', $this->creationDisplayname]);

		return $dataProvider;
	}
}
