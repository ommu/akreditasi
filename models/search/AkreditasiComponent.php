<?php
/**
 * AkreditasiComponent
 *
 * AkreditasiComponent represents the model behind the search form about `ommu\akreditasi\models\AkreditasiComponent`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\akreditasi\models\AkreditasiComponent as AkreditasiComponentModel;

class AkreditasiComponent extends AkreditasiComponentModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'publish', 'cat_id', 'parent_id', 'bobot', 'order', 'creation_id', 'modified_id'], 'integer'],
			[['component_name', 'creation_date', 'modified_date', 'updated_date', 'parentName', 'categoryName', 'creationDisplayname', 'modifiedDisplayname'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = AkreditasiComponentModel::find()->alias('t');
        } else {
            $query = AkreditasiComponentModel::find()->alias('t')
                ->select($column);
        }
		$query->joinWith([
			// 'parent parent', 
			// 'category category', 
			// 'creation creation', 
			// 'modified modified'
		]);
        if ((isset($params['sort']) && in_array($params['sort'], ['parent_id', '-parent_id', 'parentName', '-parentName'])) || (isset($params['parentName']) && $params['parentName'] != '')) {
            $query->joinWith(['parent parent']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['cat_id', '-cat_id', 'categoryName', '-categoryName'])) || (isset($params['categoryName']) && $params['categoryName'] != '')) {
            $query->joinWith(['category category']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['creationDisplayname', '-creationDisplayname'])) || (isset($params['creationDisplayname']) && $params['creationDisplayname'] != '')) {
            $query->joinWith(['creation creation']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['modifiedDisplayname', '-modifiedDisplayname'])) || (isset($params['modifiedDisplayname']) && $params['modifiedDisplayname'] != '')) {
            $query->joinWith(['modified modified']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['cat_id'] = [
			'asc' => ['category.category_name' => SORT_ASC],
			'desc' => ['category.category_name' => SORT_DESC],
		];
		$attributes['categoryName'] = [
			'asc' => ['category.category_name' => SORT_ASC],
			'desc' => ['category.category_name' => SORT_DESC],
		];
		$attributes['parent_id'] = [
			'asc' => ['parent.component_name' => SORT_ASC],
			'desc' => ['parent.component_name' => SORT_DESC],
		];
		$attributes['parentName'] = [
			'asc' => ['parent.component_name' => SORT_ASC],
			'desc' => ['parent.component_name' => SORT_DESC],
		];
		$attributes['creationDisplayname'] = [
			'asc' => ['creation.displayname' => SORT_ASC],
			'desc' => ['creation.displayname' => SORT_DESC],
		];
		$attributes['modifiedDisplayname'] = [
			'asc' => ['modified.displayname' => SORT_ASC],
			'desc' => ['modified.displayname' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
		$query->andFilterWhere([
			't.id' => $this->id,
			't.cat_id' => isset($params['category']) ? $params['category'] : $this->cat_id,
			't.parent_id' => isset($params['parent']) ? $params['parent'] : $this->parent_id,
			't.bobot' => $this->bobot,
			't.order' => $this->order,
			'cast(t.creation_date as date)' => $this->creation_date,
			't.creation_id' => isset($params['creation']) ? $params['creation'] : $this->creation_id,
			'cast(t.modified_date as date)' => $this->modified_date,
			't.modified_id' => isset($params['modified']) ? $params['modified'] : $this->modified_id,
			'cast(t.updated_date as date)' => $this->updated_date,
		]);

        if (isset($params['trash'])) {
            $query->andFilterWhere(['NOT IN', 't.publish', [0,1]]);
        } else {
            if (!isset($params['publish']) || (isset($params['publish']) && $params['publish'] == '')) {
                $query->andFilterWhere(['IN', 't.publish', [0,1]]);
            } else {
                $query->andFilterWhere(['t.publish' => $this->publish]);
            }
        }

		$query->andFilterWhere(['like', 't.component_name', $this->component_name])
			->andFilterWhere(['like', 'parent.component_name', $this->parentName])
			->andFilterWhere(['like', 'category.category_name', $this->categoryName])
			->andFilterWhere(['like', 'creation.displayname', $this->creationDisplayname])
			->andFilterWhere(['like', 'modified.displayname', $this->modifiedDisplayname]);

		return $dataProvider;
	}
}
