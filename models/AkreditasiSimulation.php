<?php
/**
 * AkreditasiSimulation
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:54 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_simulation".
 *
 * The followings are the available columns in table "ommu_akreditasi_simulation":
 * @property integer $id
 * @property integer $publish
 * @property integer $library_id
 * @property integer $user_id
 * @property string $simulation_start
 * @property string $simulation_end
 * @property integer $simulation_score
 * @property string $simulation_document
 * @property string $generated_date
 * @property integer $step_component_id
 * @property string $step_component_date
 * @property integer $step_component_score
 * @property string $creation_date
 * @property string $modified_date
 * @property integer $modified_id
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property AkreditasiAssessment[] $assessments
 * @property AkreditasiLibrary $library
 * @property AkreditasiComponent $component
 * @property AkreditasiSimulationHistory[] $histories
 * @property Users $user
 * @property Users $modified
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class AkreditasiSimulation extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;
	use \ommu\traits\FileTrait;

	public $gridForbiddenColumn = ['simulation_end', 'simulation_score', 'generated_date', 'creation_date', 'modified_date', 'modifiedDisplayname', 'updated_date', 'assessments'];

	public $libraryName;
	public $userDisplayname;
	public $modifiedDisplayname;
    public $history;
	public $completed;
    public $accreditation;
    // document
	public $document;
	public $regenerate;
    public $librarian;
	public $backToManage;

	const SCENARIO_DOCUMENT = 'documentForm';

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_simulation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['library_id', 'user_id'], 'required'],
			[['generated_date'], 'required', 'on' => self::SCENARIO_DOCUMENT],
			[['publish', 'library_id', 'user_id', 'simulation_score', 'step_component_id', 'step_component_score', 'modified_id', 'backToManage'], 'integer'],
			//[['simulation_document'], 'json'],
			[['simulation_start', 'simulation_end', 'simulation_document', 'step_component_date', 'regenerate', 'librarian', 'backToManage'], 'safe'],
			[['library_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiLibrary::className(), 'targetAttribute' => ['library_id' => 'id']],
			[['step_component_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiComponent::className(), 'targetAttribute' => ['step_component_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_DOCUMENT] = ['simulation_document', 'generated_date', 'regenerate', 'librarian'];
		return $scenarios;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'publish' => Yii::t('app', 'Publish'),
			'library_id' => Yii::t('app', 'Library'),
			'user_id' => Yii::t('app', 'User'),
			'simulation_start' => Yii::t('app', 'Simulation Start'),
			'simulation_end' => Yii::t('app', 'Simulation End'),
			'simulation_document' => Yii::t('app', 'Simulation Document'),
			'generated_date' => Yii::t('app', 'Generated Date Document'),
			'simulation_score' => Yii::t('app', 'Score'),
			'step_component_id' => Yii::t('app', 'Steps'),
			'step_component_date' => Yii::t('app', 'Last Step Date'),
			'step_component_score' => Yii::t('app', 'Last Step Score'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'assessments' => Yii::t('app', 'Assessments'),
			'libraryName' => Yii::t('app', 'Library'),
			'userDisplayname' => Yii::t('app', 'User'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
			'history' => Yii::t('app', 'History'),
			'completed' => Yii::t('app', 'Completed'),
			'accreditation' => Yii::t('app', 'Accreditation'),
			'regenerate' => Yii::t('app', 'Regenerate'),
			'librarian' => Yii::t('app', 'Librarian'),
			'librarian[headDepartment]' => Yii::t('app', 'Head Department'),
			'librarian[headLibrary]' => Yii::t('app', 'Head Library'),
			'librarian[librarian]' => Yii::t('app', 'Librarian'),
			'backToManage' => Yii::t('app', 'Back to Manage'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAssessments($count=false)
	{
        if ($count == false) {
            return $this->hasMany(AkreditasiAssessment::className(), ['simulation_id' => 'id']);
        }

		$model = AkreditasiAssessment::find()
            ->alias('t')
            ->where(['simulation_id' => $this->id]);
		$assessments = $model->count();

		return $assessments ? $assessments : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLibrary()
	{
		return $this->hasOne(AkreditasiLibrary::className(), ['id' => 'library_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getComponent()
	{
		return $this->hasOne(AkreditasiComponent::className(), ['id' => 'step_component_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getHistories($count=false)
	{
        if ($count == false) {
            return $this->hasMany(AkreditasiSimulationHistory::className(), ['simulation_id' => 'id']);
        }

		$model = AkreditasiSimulationHistory::find()
            ->alias('t')
            ->where(['simulation_id' => $this->id]);
		$histories = $model->count();

		return $histories ? $histories : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiSimulation the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiSimulation(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['libraryName'] = [
			'attribute' => 'libraryName',
			'value' => function($model, $key, $index, $column) {
				return isset($model->library) ? $model->library->library_name : '-';
				// return $model->libraryName;
			},
			'visible' => !Yii::$app->request->get('library') ? true : false,
		];
		$this->templateColumns['userDisplayname'] = [
			'attribute' => 'userDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->user) ? $model->user->displayname : '-';
				// return $model->userDisplayname;
			},
			'visible' => !Yii::$app->request->get('user') ? true : false,
		];
		$this->templateColumns['simulation_start'] = [
            'attribute' => 'simulation_start',
            'header' => Yii::t('app', 'Simulation Date'),
			'value' => function($model, $key, $index, $column) {
                return $model->parseSimulationDate();
			},
			'filter' => $this->filterDatepicker($this, 'simulation_start'),
			'format' => 'html',
		];
		$this->templateColumns['simulation_end'] = [
			'attribute' => 'simulation_end',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->simulation_end, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'simulation_end'),
		];
		$this->templateColumns['simulation_score'] = [
			'attribute' => 'simulation_score',
			'value' => function($model, $key, $index, $column) {
				return $model->simulation_score ? $model->simulation_score : '-';
			},
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['accreditation'] = [
			'attribute' => 'accreditation',
			'value' => function($model, $key, $index, $column) {
                if ($model->completed == 0) {
                    return '-';
                }
				return AkreditasiSimulation::getAccreditations('large', $model->accreditation).'<hr class="mt-4 mb-4">'.Yii::t('app', 'Score {score}', ['score' => $model->simulation_score]);
			},
			'filter' => AkreditasiSimulation::getAccreditations('small'),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'html',
		];
		$this->templateColumns['history'] = [
            'attribute' => 'history',
            'header' => $this->getAttributeLabel('step_component_id'),
			'value' => function($model, $key, $index, $column) {
                return $model->parseSimulationStep();
			},
			'filter' => $this->filterYesNo(),
			'visible' => !Yii::$app->request->get('component') ? true : false,
			'format' => 'raw',
		];
		$this->templateColumns['assessments'] = [
			'attribute' => 'assessments',
			'value' => function($model, $key, $index, $column) {
				$assessments = $model->getAssessments(true);
				return Html::a($assessments, ['o/assessment/manage', 'simulation' => $model->primaryKey], ['title' => Yii::t('app', '{count} assessments', ['count' => $assessments]), 'data-pjax' => 0]);
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
		$this->templateColumns['document'] = [
			'attribute' => 'document',
			'value' => function($model, $key, $index, $column) {
                if ($model->completed == 0) {
                    return '-';
                }
                if ($model->document == 0) {
                    return Html::a('<i class="fa fa-file"></i> '.Yii::t('app', 'generate document'), ['document', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'generate document'), 'class' => 'btn btn-info btn-sm', 'data-pjax' => 0]);
                }
				return $model::parseDocument($model->simulation_document, $model->library_id, $model->id, '<br/>', 'label');
			},
			'filter' => $this->filterYesNo(),
			'format' => 'raw',
		];
		$this->templateColumns['generated_date'] = [
			'attribute' => 'generated_date',
            'header' => Yii::t('app', 'Generated Date'),
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->generated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'generated_date'),
		];
		$this->templateColumns['completed'] = [
            'attribute' => 'completed',
			'value' => function($model, $key, $index, $column) {
                return $this->filterYesNo($model->completed);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
		$this->templateColumns['updated_date'] = [
			'attribute' => 'updated_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->updated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'updated_date'),
		];
		$this->templateColumns['publish'] = [
			'attribute' => 'publish',
			'value' => function($model, $key, $index, $column) {
				$url = Url::to(['publish', 'id' => $model->primaryKey]);
				return $this->quickAction($url, $model->publish);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !Yii::$app->request->get('trash') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * @param returnAlias set true jika ingin kembaliannya path alias atau false jika ingin string
	 * relative path. default true.
	 */
	public static function getUploadPath($returnAlias=true)
	{
		return ($returnAlias ? Yii::getAlias('@public/akreditasi') : 'akreditasi');
	}

	/**
	 * User get information
	 */
	public static function getAccreditations($type='large', $accreditation=null)
	{
        $items = [
            'a' => $type == 'large' ? Yii::t('app', 'A (Baik Sekali)') : 'A',
            'b' => $type == 'large' ? Yii::t('app', 'B (Baik)') : 'B',
            'c' => $type == 'large' ? Yii::t('app', 'C (Cukup)') : 'C',
            '0' => Yii::t('app', 'Belum Akreditasi'),
        ];

        if ($accreditation !== null) {
            return $items[$accreditation];
        }

        return $items;
	}

	/**
	 * User get information
	 */
	public static function getAccreditation($simulationScore)
	{
        if ($simulationScore < 60) {
            $accreditation = '0';
        } else if ($simulationScore >= 60 && $simulationScore <= 75) {
            $accreditation = 'c';
        } else if ($simulationScore >= 76 && $simulationScore <= 90) {
            $accreditation = 'b';
        } else if ($simulationScore >= 91 && $simulationScore <= 100) {
            $accreditation = 'a';
        }

        return $accreditation;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function setScoreFinal($id)
	{
        $model = AkreditasiSimulation::find()
            ->select(['id', 'simulation_score'])
            ->where(['id' => $id])
            ->one();

        $steps = $model->histories;
        $score = 0;
        if (is_array($steps) && !empty($steps)) {
            foreach ($steps as $key => $step) {
                $maxScore = $step->component->getIndicators('count', 1, true) * 5;
                $bobot = $step->component->bobot / 100;
                $stepScore = number_format(($step->step_component_score / $maxScore) * $bobot, 2);
                $score = $score + $stepScore;
            }
        }

        $model->simulation_score = $score * 100;
        $model->save(false, ['simulation_score']);

        return;
	}

	/**
	 * {@inheritdoc}
	 */
	public function parseSimulationDate($update=true)
	{
        if (!isset($this->library)) {
            if ($update == true) {
                return Html::a('<i class="fa fa-pencil"></i> '.Yii::t('app', 'Update'), ['admin/update', 'id' => $this->primaryKey], ['title' => Yii::t('app', 'Update Simulation'), 'class' => 'btn btn-warning btn-xs modal-btn']);
            }
        }

        if (Yii::$app->formatter->asDatetime($this->simulation_start, 'medium') == '-') {
            return Html::a('<i class="fa fa-paper-plane"></i> '.Yii::t('app', 'Start'), ['admin/run', 'id' => $this->primaryKey], ['title' => Yii::t('app', 'Start Simulation'), 'class' => 'btn btn-success btn-xs']);

        } else {
            $data = $parse = [];
            $data['simulation_start'] = $this->simulation_start;
            $data['simulation_end'] = $this->simulation_end;
            foreach ($data as $key => $val) {
                $parse[$key] = Yii::t('app', '{attribute}: {date}', [
                    'attribute' => $key == 'simulation_start' ? Yii::t('app', 'Start') : Yii::t('app', 'End'),
                    'date' => Yii::$app->formatter->asDatetime($val, 'medium'),
                ]);
            }

            if ($this->completed == 1) {
                return Html::ul($parse, ['encode' => false, 'class' => 'list-boxed']);
            }

            return join('', [
                Html::ul($parse, ['encode' => false, 'class' => 'list-boxed']),
                '<hr class="mt-4 mb-4"/>',
                Html::a('<i class="fa fa-play-circle"></i> '.Yii::t('app', 'Continue'), ['admin/run', 'id' => $this->primaryKey], ['title' => Yii::t('app', 'Start Continue'), 'class' => 'btn btn-success btn-xs']),
            ]);
        }
	}

	/**
	 * {@inheritdoc}
	 */
	public function parseSimulationStep()
	{
        if (!isset($this->library)) {
            return '-';
        }

        $componentParent = $this->library->category->componentParent;
        $oldStep = ArrayHelper::map($this->histories, 'id', 'step_component_id');

        if (!empty($oldStep)) {
            foreach ($oldStep as $key => $step) {
                $componentParent[$step] = Html::a('<i class="fa fa-check"></i>', ['o/step/view', 'id' => $key], ['title' => Yii::t('app', 'Detail Step'), 'class' => 'btn btn-success btn-xs modal-btn']).$componentParent[$step];
            }
        }

        $histories = $this->getHistories(true);
        if ($histories) {
            return join('', [
                Html::ol($componentParent, ['encode' => false, 'class' => 'list-boxed']),
                '<hr class="mt-4 mb-4"/>',
                Html::a(Yii::t('app', 'All Steps'), ['o/step/manage', 'simulation' => $this->primaryKey], ['title' => Yii::t('app', 'All Steps'), 'data-pjax' => 0]),
            ]);
        }
        
        return Html::ol($componentParent, ['encode' => false, 'class' => 'list-boxed']);
    }

	/**
	 * {@inheritdoc}
	 */
	public static function parseDocument($simulationDocument, $libraryId, $simulationId, $sep='li', $resultType='fileName')
	{
        if (!is_array($simulationDocument) || (is_array($simulationDocument) && empty($simulationDocument))) {
            return '-';
        }
        $items = self::getDocumentUrl($simulationDocument, $libraryId, true, $resultType);

        if ($sep == 'li') {
            $archive = '';
            if (array_key_exists('archive', $simulationDocument)) {
                $archive = $simulationDocument['archive'];
            }
            unset($items['archive']);

            $docs = Html::ul($items, ['item' => function($item, $index) {
				return Html::tag('li', $item);
			}, 'class' => 'list-boxed']);

            $zip = Html::a($archive ? Yii::t('app', 'Download ZIP ({archive})', ['archive' => $archive]) : Yii::t('app', 'Download ZIP'), Url::to(['archive', 'id' => $simulationId]), ['title' => Yii::t('app', 'Download ZIP'), 'class' => 'btn btn-success btn-sm', 'target' => '_blank', 'data-pjax' => 0]);

			return join('<hr/>', [$docs, $zip]);
		}

		return implode($sep, $items);
    }

	/**
	 * {@inheritdoc}
	 */
	public static function getDocumentUrl($simulationDocument, $libraryId, $hyperlink=false, $resultType='fileName')
	{
        if ($hyperlink) {
            $documentPath = join('/', [self::getUploadPath(false), 'document']);
            $uploadPath = join('/', [$documentPath, $libraryId]);
        }

		$items = [];
		foreach ($simulationDocument as $key => $val) {
            if ($hyperlink) {
                $class = 'btn btn-success btn-sm';
                if ($resultType == 'fileName') {
                    $text = $val;
                    $class = '';
                } else if ($resultType == 'fileNameWithLabel') {
                    $text = $key.' ('.$val.')';
                } else if ($resultType == 'label') {
                    $text = $key;
                }
                $items[$key] = Html::a($text, join('/', ['@webpublic', $uploadPath, $val]), ['title' => $key.': '.$val, 'class' => $class, 'target' => '_blank', 'data-pjax' => 0]);

            } else {
                $items[$key] = join('/', [self::getUploadPath(), 'document', $libraryId, $val]);
            }
		}
		// return ($returnAlias ? Yii::getAlias('@public/akreditasi') : 'akreditasi');

		return $items;
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

        if ($this->simulation_document == '') {
            $this->simulation_document = [];
        } else {
            $this->simulation_document = Json::decode($this->simulation_document);
        }
		// $this->libraryName = isset($this->library) ? $this->library->library_name : '-';
		// $this->userDisplayname = isset($this->user) ? $this->user->displayname : '-';
		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';
		$this->history = $this->getHistories(true) ? 1 : 0;
		$this->completed = Yii::$app->formatter->asDatetime($this->simulation_end, 'medium') != '-' ? 1 : 0;
		$this->accreditation = $this::getAccreditation($this->simulation_score);
		$this->document = !empty($this->simulation_document) ? 1 : 0;
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->user_id == null) {
                    $this->user_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            } else {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
            $this->simulation_start = Yii::$app->formatter->asDate($this->simulation_start, 'php:Y-m-d H:i:s');
            $this->simulation_end = Yii::$app->formatter->asDate($this->simulation_end, 'php:Y-m-d H:i:s');
			$this->simulation_document = Json::encode($this->simulation_document);
            $this->generated_date = Yii::$app->formatter->asDate($this->generated_date, 'php:Y-m-d H:i:s');
            $this->step_component_date = Yii::$app->formatter->asDate($this->step_component_date, 'php:Y-m-d H:i:s');
        }
        return true;
	}
}
