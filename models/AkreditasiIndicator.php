<?php
/**
 * AkreditasiIndicator
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:32 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_indicator".
 *
 * The followings are the available columns in table "ommu_akreditasi_indicator":
 * @property integer $id
 * @property integer $publish
 * @property integer $component_id
 * @property string $question
 * @property string $answer
 * @property integer $order
 * @property string $creation_date
 * @property integer $creation_id
 * @property string $modified_date
 * @property integer $modified_id
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property AkreditasiComponent $component
 * @property Users $creation
 * @property Users $modified
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use app\models\Users;

class AkreditasiIndicator extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;

	public $gridForbiddenColumn = ['order', 'creation_date', 'creationDisplayname', 'modified_date', 'modifiedDisplayname', 'updated_date'];

	public $componentName;
	public $creationDisplayname;
	public $modifiedDisplayname;
	public $categoryId;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_indicator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['component_id', 'question', 'answer'], 'required'],
			[['publish', 'component_id', 'order', 'creation_id', 'modified_id'], 'integer'],
			[['question'], 'string'],
			[['publish', 'answer', 'order'], 'safe'],
			[['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiComponent::className(), 'targetAttribute' => ['component_id' => 'id']],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'publish' => Yii::t('app', 'Publish'),
			'component_id' => Yii::t('app', 'Component'),
			'question' => Yii::t('app', 'Question'),
			'answer' => Yii::t('app', 'Answer'),
			'order' => Yii::t('app', 'Order'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'answers' => Yii::t('app', 'Answers'),
			'componentName' => Yii::t('app', 'Component'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
			'categoryId' => Yii::t('app', 'Category'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getComponent()
	{
		return $this->hasOne(AkreditasiComponent::className(), ['id' => 'component_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiIndicator the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiIndicator(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['component_id'] = [
			'attribute' => 'component_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->component) ? $model->component->component_name : '-';
				// return $model->componentName;
			},
			'filter' => AkreditasiComponent::getComponent(),
			'visible' => !Yii::$app->request->get('component') ? true : false,
		];
		$this->templateColumns['question'] = [
			'attribute' => 'question',
			'value' => function($model, $key, $index, $column) {
				return $model->question;
			},
		];
		$this->templateColumns['answer'] = [
			'attribute' => 'answer',
			'value' => function($model, $key, $index, $column) {
				return self::parseAnswer($model->answer);
			},
			'format' => 'html',
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
		$this->templateColumns['updated_date'] = [
			'attribute' => 'updated_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->updated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'updated_date'),
		];
		$this->templateColumns['order'] = [
			'attribute' => 'order', 
			'value' => function($model, $key, $index, $column) {
				return $model->order;
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['publish'] = [
			'attribute' => 'publish',
			'value' => function($model, $key, $index, $column) {
				$url = Url::to(['publish', 'id' => $model->primaryKey]);
				return $this->quickAction($url, $model->publish);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !Yii::$app->request->get('trash') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * function getChoices
	 */
	public static function getChoices()
	{
		$items = array(
			'a' => 'A',
			'b' => 'B',
			'c' => 'C',
			'd' => 'D',
			'e' => 'E',
		);

		return $items;
	}

	/**
	 * function parseAnswer
	 */
	public static function parseAnswer($answer, $sep='li')
	{
        if (!is_array($answer) || (is_array($answer) && empty($answer))) {
            return '-';
        }

        if ($sep == 'li') {
			return Html::ul($answer, ['item' => function($item, $index) {
				return Html::tag('li', strtoupper($index).'. '.$item);
			}, 'class' => 'list-unstyled']);
        }

		return implode($sep, $answer);
	}

	/**
	 * function getAnswerStatus
	 */
	public function getAnswerStatus()
	{
		$answer = $this->answer;
		$return = true;

        if (!is_array($answer)) {
            return false;
        }

		foreach ($answer as $key => $val) {
            if (!$val) {
                $return = false;
            }
		}

		return $return;
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		$this->answer = Json::decode($this->answer);
		// $this->componentName = isset($this->component) ? $this->component->component_name : '-';
		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            } else {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }

            if ($this->getAnswerStatus() == false) {
                $this->addError('answer', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('answer')]));
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
			$this->answer = Json::encode($this->answer);
        }
        return true;
	}
}
