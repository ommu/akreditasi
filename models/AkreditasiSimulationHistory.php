<?php
/**
 * AkreditasiSimulationHistory
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 24 December 2020, 13:46 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_simulation_history".
 *
 * The followings are the available columns in table "ommu_akreditasi_simulation_history":
 * @property integer $id
 * @property integer $simulation_id
 * @property integer $user_id
 * @property integer $step_component_id
 * @property string $step_component_date
 * @property integer $step_component_score
 *
 * The followings are the available model relations:
 * @property AkreditasiSimulation $simulation
 * @property Users $user
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use app\models\Users;

class AkreditasiSimulationHistory extends \app\components\ActiveRecord
{
	public $gridForbiddenColumn = [];

	public $simulations;
	public $userDisplayname;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_simulation_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['simulation_id', 'user_id', 'step_component_id'], 'required'],
			[['simulation_id', 'user_id', 'step_component_id', 'step_component_score'], 'integer'],
			[['step_component_date'], 'safe'],
			[['simulation_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiSimulation::className(), 'targetAttribute' => ['simulation_id' => 'id']],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'simulation_id' => Yii::t('app', 'Simulation'),
			'user_id' => Yii::t('app', 'User'),
			'step_component_id' => Yii::t('app', 'Step'),
			'step_component_date' => Yii::t('app', 'Step Date'),
			'step_component_score' => Yii::t('app', 'Step Score'),
			'simulations' => Yii::t('app', 'Simulation'),
			'userDisplayname' => Yii::t('app', 'User'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSimulation()
	{
		return $this->hasOne(AkreditasiSimulation::className(), ['id' => 'simulation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getComponent()
	{
		return $this->hasOne(AkreditasiComponent::className(), ['id' => 'step_component_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiSimulationHistory the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiSimulationHistory(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
		parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['simulations'] = [
			'attribute' => 'simulations',
			'value' => function($model, $key, $index, $column) {
				return $model->simulation->parseSimulationDate(false);
				// return $model->simulations;
			},
			'visible' => !Yii::$app->request->get('simulation') ? true : false,
			'format' => 'html',
		];
		$this->templateColumns['userDisplayname'] = [
			'attribute' => 'userDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->user) ? $model->user->displayname : '-';
				// return $model->userDisplayname;
			},
			'visible' => !Yii::$app->request->get('user') ? true : false,
		];
		$this->templateColumns['step_component_id'] = [
			'attribute' => 'step_component_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->component) ? $model->component->component_name : '-';
			},
		];
		$this->templateColumns['step_component_date'] = [
			'attribute' => 'step_component_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->step_component_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'step_component_date'),
		];
		$this->templateColumns['step_component_score'] = [
			'attribute' => 'step_component_score',
			'value' => function($model, $key, $index, $column) {
				return $model->step_component_score;
			},
			'contentOptions' => ['class' => 'text-center'],
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getStepScore($simulationId, $componentId)
	{
        $model = self::find()
            ->select(['step_component_score'])
            ->where(['simulation_id' => $simulationId])
            ->andWhere(['step_component_id' => $componentId])
            ->orderBy('id DESC')
            ->one();

        return $model->step_component_score;
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		// $this->simulations = isset($this->simulation) ? $this->simulation->library->library_name : '-';
		// $this->userDisplayname = isset($this->user) ? $this->user->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->user_id == null) {
                    $this->user_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
			$this->step_component_date = Yii::$app->formatter->asDate($this->step_component_date, 'php:Y-m-d');
        }
        return true;
	}
}
