<?php
/**
 * AkreditasiComponent
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:25 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_component".
 *
 * The followings are the available columns in table "ommu_akreditasi_component":
 * @property integer $id
 * @property integer $publish
 * @property integer $cat_id
 * @property integer $parent_id
 * @property string $component_name
 * @property integer $bobot
 * @property integer $order
 * @property string $creation_date
 * @property integer $creation_id
 * @property string $modified_date
 * @property integer $modified_id
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property AkreditasiComponent $parent
 * @property AkreditasiComponent[] $components
 * @property AkreditasiCategory $category
 * @property AkreditasiIndicator[] $indicators
 * @property Users $creation
 * @property Users $modified
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Users;

class AkreditasiComponent extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;

	public $gridForbiddenColumn = ['order', 'creation_date', 'creationDisplayname', 'modified_date', 'modifiedDisplayname', 'updated_date'];

	public $parentName;
	public $categoryName;
	public $creationDisplayname;
	public $modifiedDisplayname;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_component';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['cat_id', 'component_name'], 'required'],
			[['publish', 'cat_id', 'parent_id', 'bobot', 'order', 'creation_id', 'modified_id'], 'integer'],
			[['publish', 'parent_id', 'bobot', 'order'], 'safe'],
			[['component_name'], 'string', 'max' => 64],
			[['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiComponent::className(), 'targetAttribute' => ['parent_id' => 'id']],
			[['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiCategory::className(), 'targetAttribute' => ['cat_id' => 'id']],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'publish' => Yii::t('app', 'Publish'),
			'cat_id' => Yii::t('app', 'Category'),
			'parent_id' => Yii::t('app', 'Parent'),
			'component_name' => Yii::t('app', 'Component'),
			'bobot' => Yii::t('app', 'Bobot'),
			'order' => Yii::t('app', 'Order'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'components' => Yii::t('app', 'Subs'),
			'indicators' => Yii::t('app', 'Indicators'),
			'parentName' => Yii::t('app', 'Parent'),
			'categoryName' => Yii::t('app', 'Category'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
		];
	}

	/**
	 * @param $type relation|array|dataProvider|count
	 * @return \yii\db\ActiveQuery
	 */
	public function getSubComponents($type='relation', $publish=1)
	{
        if ($type == 'relation') {
            return $this->hasMany(AkreditasiComponent::className(), ['parent_id' => 'id'])
                ->alias('components')
                ->andOnCondition([sprintf('%s.publish', 'components') => $publish]);
        }

        if ($type == 'array') {
            return \yii\helpers\ArrayHelper::map($this->subComponents, 'component_name', 'component_name');
        }

        if ($type == 'dataProvider') {
			return new \yii\data\ActiveDataProvider([
				'query' => $this->getSubComponents('relation', $publish),
			]);
		}

		$model = AkreditasiComponent::find()
            ->alias('t')
            ->where(['parent_id' => $this->id]);
        if ($publish == 0) {
            $model->unpublish();
        } else if ($publish == 1) {
            $model->published();
        } else if ($publish == 2) {
            $model->deleted();
        }
		$components = $model->count();

		return $components ? $components : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent()
	{
		return $this->hasOne(AkreditasiComponent::className(), ['id' => 'parent_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(AkreditasiCategory::className(), ['id' => 'cat_id']);
	}

	/**
	 * @param $type relation|array|dataProvider|count
	 * @return \yii\db\ActiveQuery
	 */
	public function getIndicators($type='relation', $publish=1, $isComponentParams=false)
	{
        if ($type == 'relation') {
            return $this->hasMany(AkreditasiIndicator::className(), ['component_id' => 'id'])
                ->alias('indicators')
                ->andOnCondition([sprintf('%s.publish', 'indicators') => $publish]);
        }

        if ($type == 'array') {
            return \yii\helpers\ArrayHelper::map($this->indicators, 'id', 'question');
        }

        if ($type == 'dataProvider') {
			return new \yii\data\ActiveDataProvider([
				'query' => $this->getIndicators('relation', $publish),
			]);
        }
        
        if ($isComponentParams == false) {
            $model = AkreditasiIndicator::find()
                ->alias('t')
                ->where(['component_id' => $this->id]);
            if ($publish == 0) {
                $model->unpublish();
            } else if ($publish == 1) {
                $model->published();
            } else if ($publish == 2) {
                $model->deleted();
            }
            $indicators = $model->count();

        } else {
            $indicators = 0;
            $indicators = $indicators + $this->getIndicators('count');
            $subComponents = $this->subComponents;
            if (!empty($subComponents)) {
                $indicators = $indicators + $this->getIndicatorWithSubComponent($subComponents, $indicators);
            }
        }

		return $indicators ? $indicators : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiComponent the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiComponent(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['cat_id'] = [
			'attribute' => 'cat_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->category) ? $model->category->category_name : '-';
				// return $model->categoryName;
			},
			'filter' => AkreditasiCategory::getCategory(),
			'visible' => !Yii::$app->request->get('category') && !Yii::$app->request->get('parent') ? true : false,
		];
		$this->templateColumns['parent_id'] = [
			'attribute' => 'parent_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->parent) ? $model->parent->component_name : '-';
				// return $model->parentName;
			},
			//'filter' => AkreditasiComponent::getComponent(),
			'visible' => !Yii::$app->request->get('parent') ? true : false,
		];
		$this->templateColumns['component_name'] = [
			'attribute' => 'component_name',
			'value' => function($model, $key, $index, $column) {
				return $model->component_name;
			},
		];
		$this->templateColumns['components'] = [
			'attribute' => 'components',
			'value' => function($model, $key, $index, $column) {
				$components = $model->getSubComponents('array');
				$components = !$components ? '' : Html::ol($components, ['encode' => false, 'class' => 'list-boxed']).'<hr class="mt-4 mb-4"/>';
				return $components.Html::a('<i class="fa fa-plus-square"></i> '.Yii::t('app', 'Add Sub Component'), ['instrument/component/create', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Add Sub Component'), 'class' => 'btn btn-success btn-xs modal-btn']);
			},
			'filter' => false,
			'format' => 'html',
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
		$this->templateColumns['updated_date'] = [
			'attribute' => 'updated_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->updated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'updated_date'),
		];
		$this->templateColumns['bobot'] = [
			'attribute' => 'bobot',
			'value' => function($model, $key, $index, $column) {
				return !$model->parent_id ? Yii::$app->formatter->asPercent(($model->bobot/100), 2) : '-';
			},
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['indicators'] = [
			'attribute' => 'indicators',
			'value' => function($model, $key, $index, $column) {
				$indicators = $model->getIndicators('count');
				return !$model->getSubComponents('count') ? 
					Html::a($indicators, ['instrument/indicator/manage', 'component' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} indicators', ['count' => $indicators]), 'data-pjax' => 0]) : 
					$model->getIndicators('count', 1, true);
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
		$this->templateColumns['order'] = [
			'attribute' => 'order', 
			'value' => function($model, $key, $index, $column) {
				return $model->order;
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['publish'] = [
			'attribute' => 'publish',
			'value' => function($model, $key, $index, $column) {
				$url = Url::to(['publish', 'id' => $model->primaryKey]);
				return $this->quickAction($url, $model->publish);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !Yii::$app->request->get('trash') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * function getComponent
	 */
	public static function getComponent($publish=null, $array=true)
	{
		$model = self::find()->alias('t')
			->select(['t.id', 't.component_name']);
        if ($publish != null) {
            $model->andWhere(['t.publish' => $publish]);
        }

		$model = $model->orderBy('t.component_name ASC')->all();

        if ($array == true) {
            return \yii\helpers\ArrayHelper::map($model, 'id', 'component_name');
        }

		return $model;
	}
	/**
	 * {@inheritdoc}
	 */
	public function getIndicatorWithSubComponent($components, $indicators)
	{
        if (!empty($components)) {
            foreach ($components as $key => $val) {
                $indicators = $indicators + $val->getIndicators('count');
                $subComponents = $val->subComponents;
                if (!empty($subComponents)) {
                    $indicators = $this->getIndicatorWithSubComponent($subComponents, $indicators);
                }
            }
            return $indicators;
        }
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		// $this->parentName = isset($this->parent) ? $this->parent->component_name : '-';
		// $this->categoryName = isset($this->category) ? $this->category->category_name : '-';
		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if (!$this->parent_id && !$this->bobot) {
                $this->addError('bobot', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('bobot')]));
            }

            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            } else {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}
}
