<?php
/**
 * AkreditasiAssessment
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:54 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_assessment".
 *
 * The followings are the available columns in table "ommu_akreditasi_assessment":
 * @property integer $id
 * @property integer $simulation_id
 * @property integer $indicator_id
 * @property string $choice
 * @property string $creation_date
 * @property integer $creation_id
 *
 * The followings are the available model relations:
 * @property AkreditasiSimulation $simulation
 * @property AkreditasiIndicator $indicator
 * @property Users $creation
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use yii\helpers\Html;
use app\models\Users;

class AkreditasiAssessment extends \app\components\ActiveRecord
{
	public $gridForbiddenColumn = [];

	public $simulations;
	public $indicatorQuestionComponent;
	public $creationDisplayname;

	public $componentId;

	const SCENARIO_SIMULATION = 'simulationRun';
    const SCENARIO_SINGLE_CHOICE = 'singleChoice';

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_assessment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['simulation_id', 'choice'], 'required'],
			[['simulation_id', 'indicator_id', 'creation_id'], 'integer'],
			[['choice'], 'string', 'max' => 1, 'on' => self::SCENARIO_SINGLE_CHOICE],
			[['simulation_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiSimulation::className(), 'targetAttribute' => ['simulation_id' => 'id']],
			[['indicator_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiIndicator::className(), 'targetAttribute' => ['indicator_id' => 'id']],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'simulation_id' => Yii::t('app', 'Simulation'),
			'indicator_id' => Yii::t('app', 'Indicator'),
			'choice' => Yii::t('app', 'Choice'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'simulations' => Yii::t('app', 'Simulation'),
			'indicatorQuestionComponent' => Yii::t('app', 'Indicator'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SIMULATION] = ['simulation_id', 'choice', 'componentId'];
		$scenarios[self::SCENARIO_SINGLE_CHOICE] = ['simulation_id', 'indicator_id', 'choice'];
		return $scenarios;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSimulation()
	{
		return $this->hasOne(AkreditasiSimulation::className(), ['id' => 'simulation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getIndicator()
	{
		return $this->hasOne(AkreditasiIndicator::className(), ['id' => 'indicator_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiAssessment the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiAssessment(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['simulations'] = [
			'attribute' => 'simulations',
			'value' => function($model, $key, $index, $column) {
				return isset($model->simulation) ? $model->simulation->library->library_name : '-';
				// return $model->simulations;
			},
			'visible' => !Yii::$app->request->get('simulation') ? true : false,
		];
		$this->templateColumns['indicatorQuestionComponent'] = [
			'attribute' => 'indicatorQuestionComponent',
			'value' => function($model, $key, $index, $column) {
                $question = isset($model->indicator) ? $model->indicator->question : '-';
                $componentName = isset($model->indicator->component) ? $model->indicator->component->component_name : '-';
                return Html::tag('strong', $componentName).'<br/>'.$question;
				// return $model->indicatorQuestionComponent;
			},
			'visible' => !Yii::$app->request->get('indicator') ? true : false,
            'format' => 'html',
		];
		$this->templateColumns['choice'] = [
			'attribute' => 'choice',
			'value' => function($model, $key, $index, $column) {
				return $model->choice;
			},
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * insertAssessment
	 * 
	 * condition
	 * 0 = invite not null
	 * 1 = assessment save
	 * 2 = assessment not save
	 */
	public static function insertAssessment($choice, $indicator, $simulation)
	{
        $condition = 0;

        $model = new self();
        $model->scenario = self::SCENARIO_SINGLE_CHOICE;
        $model->simulation_id = $simulation;
        $model->indicator_id = $indicator;
        $model->choice = $choice;
        if ($model->save()) {
            $condition = 1;
        } else {
            $condition = 2;
        }

		return $condition;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getChoiceScore($value=null)
	{
        $scores = [
            'a' => 5,
            'b' => 4,
            'c' => 3,
            'd' => 2,
            'e' => 1,
        ];

        if ($value !== null) {
            return $scores[$value];
        }

        return $scores;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getStepScore($choices)
	{
        $score = self::getChoiceScore();

        $result = 0;
        if (is_array($choices) && !empty($choices)) {
            foreach ($choices as $key => $choice) {
                if ($choice == '') {
                    continue;
                }
                $result = $result + (int)$score[$choice];
            }
        }

        return $result;
	}

	/**
	 * function getChoice
	 */
	public static function getChoice($simulationId, $indicatorId)
	{
        $model = self::find()
            ->select(['choice'])
            ->where(['simulation_id' => $simulationId])
            ->andWhere(['indicator_id' => $indicatorId])
            ->orderBy('id DESC')
            ->one();

        return $model->choice;
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		// $this->simulations = isset($this->simulation) ? $this->simulation->library->library_name : '-';
		// $this->indicatorQuestionComponent = isset($this->indicator) ? $this->indicator->question : '-';
		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }

                if ($this->scenario === self::SCENARIO_SIMULATION) {
                    $choices = $this->choice;
                    $chioceValidate = true;
                    foreach ($choices as $key => $choice) {
                        if ($chioceValidate == false) {
                            continue;
                        }
                        if ($choice == '') {
                            $chioceValidate = false;
                        }
                    }
                    if ($chioceValidate == false) {
                        $this->addError('choice', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('choice')]));
                    }
                }
            }
        }
        return true;
	}
}
