<?php
/**
 * AkreditasiSimulationHistory
 *
 * This is the ActiveQuery class for [[\ommu\akreditasi\models\AkreditasiSimulationHistory]].
 * @see \ommu\akreditasi\models\AkreditasiSimulationHistory
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 24 December 2020, 13:46 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\query;

class AkreditasiSimulationHistory extends \yii\db\ActiveQuery
{
	/*
	public function active()
	{
		return $this->andWhere('[[status]]=1');
	}
	*/

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\AkreditasiSimulationHistory[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\AkreditasiSimulationHistory|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
