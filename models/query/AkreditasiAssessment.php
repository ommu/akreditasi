<?php
/**
 * AkreditasiAssessment
 *
 * This is the ActiveQuery class for [[\ommu\akreditasi\models\AkreditasiAssessment]].
 * @see \ommu\akreditasi\models\AkreditasiAssessment
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:54 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\models\query;

class AkreditasiAssessment extends \yii\db\ActiveQuery
{
	/*
	public function active()
	{
		return $this->andWhere('[[status]]=1');
	}
	*/

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\AkreditasiAssessment[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\AkreditasiAssessment|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
