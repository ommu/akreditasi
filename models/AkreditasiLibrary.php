<?php
/**
 * AkreditasiLibrary
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:53 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 * This is the model class for table "ommu_akreditasi_library".
 *
 * The followings are the available columns in table "ommu_akreditasi_library":
 * @property integer $id
 * @property integer $publish
 * @property integer $cat_id
 * @property string $library_name
 * @property string $npp
 * @property string $library_address
 * @property string $library_contact
 * @property integer $library_city
 * @property string $accreditation
 * @property string $creation_date
 * @property integer $creation_id
 * @property string $modified_date
 * @property integer $modified_id
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property AkreditasiCategory $category
 * @property CoreZoneCity $city
 * @property AkreditasiOfficer[] $officers
 * @property AkreditasiSimulation[] $simulations
 * @property Users $creation
 * @property Users $modified
 *
 */

namespace ommu\akreditasi\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\Users;
use ommu\core\models\CoreZoneCity;
use yii\helpers\ArrayHelper;

class AkreditasiLibrary extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;

	public $gridForbiddenColumn = ['library_address', 'library_contact', 'creation_date', 'creationDisplayname', 'modified_date', 'modifiedDisplayname', 'updated_date'];

	public $categoryName;
	public $cityName;
	public $creationDisplayname;
	public $modifiedDisplayname;
	public $officer;
	public $simulation;
	public $backToManage;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_akreditasi_library';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['cat_id', 'library_name'], 'required'],
			[['publish', 'cat_id', 'library_city', 'creation_id', 'modified_id', 'backToManage'], 'integer'],
			[['library_name', 'npp', 'accreditation'], 'string'],
			//[['library_address', 'library_contact'], 'json'],
			[['library_name'], 'unique'],
			[['npp', 'library_address', 'library_contact', 'accreditation', 'backToManage'], 'safe'],
			[['accreditation'], 'string', 'max' => 1],
			[['npp'], 'string', 'max' => 32],
			[['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => AkreditasiCategory::className(), 'targetAttribute' => ['cat_id' => 'id']],
			[['library_city'], 'exist', 'skipOnError' => true, 'targetClass' => CoreZoneCity::className(), 'targetAttribute' => ['library_city' => 'city_id']],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'publish' => Yii::t('app', 'Publish'),
			'cat_id' => Yii::t('app', 'Category'),
			'library_name' => Yii::t('app', 'Library'),
			'npp' => Yii::t('app', 'No.NPP'),
			'library_address' => Yii::t('app', 'Address'),
			'library_address[place]' => Yii::t('app', 'Place'),
			'library_address[country]' => Yii::t('app', 'Country'),
			'library_address[province]' => Yii::t('app', 'Province'),
			'library_address[city]' => Yii::t('app', 'City'),
			'library_address[district]' => Yii::t('app', 'District'),
			'library_address[village]' => Yii::t('app', 'Village'),
			'library_address[zipcode]' => Yii::t('app', 'Zipcode'),
			'library_contact' => Yii::t('app', 'Contact'),
			'library_contact[phone]' => Yii::t('app', 'Phone'),
			'library_contact[fax]' => Yii::t('app', 'FAX'),
			'library_contact[email]' => Yii::t('app', 'Email'),
			'library_contact[hotline]' => Yii::t('app', 'Hotline'),
			'library_contact[website]' => Yii::t('app', 'Website'),
			'library_city' => Yii::t('app', 'City'),
			'accreditation' => Yii::t('app', 'Accreditation'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'categoryName' => Yii::t('app', 'Category'),
			'cityName' => Yii::t('app', 'City'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
			'address' => Yii::t('app', 'Address'),
			'contact' => Yii::t('app', 'Contact'),
			'officer' => Yii::t('app', 'Librarian'),
			'simulation' => Yii::t('app', 'Simulation'),
			'backToManage' => Yii::t('app', 'Back to Manage'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(AkreditasiCategory::className(), ['id' => 'cat_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCity()
	{
		return $this->hasOne(CoreZoneCity::className(), ['city_id' => 'library_city']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOfficers($count=false, $publish=1, $officerLevel='all')
	{
        if ($count == false) {
            $model = $this->hasMany(AkreditasiOfficer::className(), ['library_id' => 'id'])
                ->alias('officers');
            if ($officerLevel == 'all') {
                return $model->andOnCondition([sprintf('%s.publish', 'officers') => $publish]);
            } else {
                return $model->andOnCondition([sprintf('%s.publish', 'officers') => $publish, sprintf('%s.officer_level', 'officers') => $officerLevel]);
            }
        }

		$model = AkreditasiOfficer::find()
            ->alias('t')
            ->where(['library_id' => $this->id]);
        if ($publish == 0) {
            $model->unpublish();
        } else if ($publish == 1) {
            $model->published();
        } else if ($publish == 2) {
            $model->deleted();
        }
        if ($officerLevel != 'all') {
            $model->andWhere(['officer_level' => $officerLevel]);
        }
		$officers = $model->count();

		return $officers ? $officers : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSimulations($count=false, $publish=1)
	{
        if ($count == false) {
            return $this->hasMany(AkreditasiSimulation::className(), ['library_id' => 'id'])
                ->alias('simulations')
                ->andOnCondition([sprintf('%s.publish', 'simulations') => $publish]);
        }

		$model = AkreditasiSimulation::find()
            ->alias('t')
            ->where(['library_id' => $this->id]);
        if ($publish == 0) {
            $model->unpublish();
        } else if ($publish == 1) {
            $model->published();
        } else if ($publish == 2) {
            $model->deleted();
        }
		$simulations = $model->count();

		return $simulations ? $simulations : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\akreditasi\models\query\AkreditasiLibrary the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\akreditasi\models\query\AkreditasiLibrary(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['library_name'] = [
			'attribute' => 'library_name',
			'value' => function($model, $key, $index, $column) {
				return $model->library_name;
			},
		];
		$this->templateColumns['npp'] = [
			'attribute' => 'npp',
			'value' => function($model, $key, $index, $column) {
				return $model->npp;
			},
		];
		$this->templateColumns['cat_id'] = [
			'attribute' => 'cat_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->category) ? $model->category->category_name : '-';
				// return $model->categoryName;
			},
			'filter' => AkreditasiCategory::getCategory(),
			'visible' => !Yii::$app->request->get('category') ? true : false,
		];
		$this->templateColumns['cityName'] = [
			'attribute' => 'cityName',
			'value' => function($model, $key, $index, $column) {
				return isset($model->city) ? $model->city->city_name : '-';
				// return $model->cityName;
			},
			'visible' => !Yii::$app->request->get('city') ? true : false,
		];
		$this->templateColumns['accreditation'] = [
			'attribute' => 'accreditation',
			'value' => function($model, $key, $index, $column) {
				return AkreditasiSimulation::getAccreditations('large', $model->accreditation);
			},
			'filter' => ArrayHelper::merge(AkreditasiSimulation::getAccreditations('small'), ['-' => Yii::t('app', '- no input -')]),
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['library_address'] = [
			'attribute' => 'library_address',
			'value' => function($model, $key, $index, $column) {
                return $this::parseAddress($model->library_address);
			},
		];
		$this->templateColumns['library_contact'] = [
			'attribute' => 'library_contact',
			'value' => function($model, $key, $index, $column) {
                return $this::parseContact($model->library_contact);
			},
            'format' => 'html',
		];
		$this->templateColumns['officer'] = [
			'attribute' => 'officer',
			'value' => function($model, $key, $index, $column) {
				$officers = $model->getOfficers(true);
				return Html::a($officers, ['library/officer/manage', 'library' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} officers', ['count' => $officers]), 'data-pjax' => 0]);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
		$this->templateColumns['simulation'] = [
			'attribute' => 'simulation',
			'value' => function($model, $key, $index, $column) {
				$simulations = $model->getSimulations(true);
				return Html::a($simulations, ['admin/manage', 'library' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} simulations', ['count' => $simulations]), 'data-pjax' => 0]);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
		$this->templateColumns['updated_date'] = [
			'attribute' => 'updated_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->updated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'updated_date'),
		];
		$this->templateColumns['publish'] = [
			'attribute' => 'publish',
			'value' => function($model, $key, $index, $column) {
				$url = Url::to(['publish', 'id' => $model->primaryKey]);
				return $this->quickAction($url, $model->publish);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !Yii::$app->request->get('trash') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

        if ($this->library_address == '') {
            $this->library_address = [];
        } else {
            $this->library_address = Json::decode($this->library_address);
        }
        if ($this->library_contact == '') {
            $this->library_contact = [];
        } else {
            $this->library_contact = Json::decode($this->library_contact);
        }
		// $this->categoryName = isset($this->category) ? $this->category->category_name : '-';
		// $this->cityName = isset($this->city) ? $this->city->city_name : '-';
		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';
		$this->officer = $this->getOfficers(true) ? 1 : 0;
		$this->simulation = $this->getSimulations(true) ? 1 : 0;
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            } else {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
            if (is_array($this->library_address)) {
                $this->library_address = Json::encode($this->library_address);
            }
            if (is_array($this->library_contact)) {
                $this->library_contact = Json::encode($this->library_contact);
            }
        }
        return true;
	}
}
