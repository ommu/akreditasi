<?php
/**
 * LibrarySuggestAction
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 23 November 2020, 16:31 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 */

namespace ommu\akreditasi\actions;

use Yii;
use ommu\akreditasi\models\AkreditasiLibrary;

class LibrarySuggestAction extends \yii\base\Action
{
	/**
	 * {@inheritdoc}
	 */
	protected function beforeRun()
	{
        if (parent::beforeRun()) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			Yii::$app->response->charset = 'UTF-8';
        }
        return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function run()
	{
		$term = utf8_decode(urldecode(Yii::$app->request->get('term')));

        if ($term == null) return [];

		$model = AkreditasiLibrary::find()
            ->alias('t')
            ->joinWith([
                'city city', 
            ])
			->suggest()
			->andWhere(['or',
				['like', 't.library_name', $term],
				['like', 'city.city_name', $term]
			]);
		$model = $model->limit(15)->all();

		$result = [];
        foreach ($model as $val) {
			$result[$val->id] = [
				'id' => $val->id, 
				'name' => $val->library_name,
			];
            if ($val->library_city) {
				$result[$val->id]['city'] = $val->city->city_name;
            }
		}
		return array_values($result);
	}
}
