<?php
/**
 * akreditasi module config
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 13 August 2019, 21:15 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

return [
	'id' => 'akreditasi',
	'class' => ommu\akreditasi\Module::className(),
];