<?php
/**
 * Akreditasi Assessments (akreditasi-assessment)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\o\AssessmentController
 * @var $model ommu\akreditasi\models\AkreditasiAssessment
 * @var $searchModel ommu\akreditasi\models\search\AkreditasiAssessment
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:59 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\GridView;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
if ($simulation != null) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
	$this->params['breadcrumbs'][] = ['label' => $simulation->library->library_name, 'url' => ['library/admin/view', 'id' => $simulation->library_id], 'class' => 'modal-btn'];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['admin/manage', 'library' => $simulation->library_id]];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($simulation->simulation_start)]), 'url' => ['admin/view', 'id' => $simulation->id]];
}
$this->params['breadcrumbs'][] = Yii::t('app', 'Assessments');

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Add Assessment'), 'url' => Url::to(['create']), 'icon' => 'plus-square', 'htmlOptions' => ['class' => 'btn btn-success']],
];
$this->params['menu']['option'] = [
	//['label' => Yii::t('app', 'Search'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Grid Option'), 'url' => 'javascript:void(0);'],
];
?>

<div class="akreditasi-assessment-manage">
<?php Pjax::begin(); ?>

<?php if ($simulation != null) {
    echo $this->render('/admin/admin_view', ['model' => $simulation, 'small' => true]);
} ?>

<?php if ($indicator != null) {
    echo $this->render('/instrument/indicator/admin_view', ['model' => $indicator, 'small' => true]);
} ?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php echo $this->render('_option_form', ['model' => $searchModel, 'gridColumns' => $searchModel->activeDefaultColumns($columns), 'route' => $this->context->route]); ?>

<?php
$columnData = $columns;
array_push($columnData, [
	'class' => 'app\components\grid\ActionColumn',
	'header' => Yii::t('app', 'Option'),
	'urlCreator' => function($action, $model, $key, $index) {
        if ($action == 'view') {
            return Url::to(['view', 'id' => $key]);
        }
        if ($action == 'update') {
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'delete') {
            return Url::to(['delete', 'id' => $key]);
        }
	},
	'buttons' => [
		'view' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Assessment'), 'class' => 'modal-btn']);
		},
		'update' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Assessment')]);
		},
		'delete' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				'title' => Yii::t('app', 'Delete Assessment'),
				'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'data-method'  => 'post',
			]);
		},
	],
	'template' => '{view} {delete}',
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columnData,
]); ?>

<?php Pjax::end(); ?>
</div>