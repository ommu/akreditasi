<?php
/**
 * Akreditasi Assessments (akreditasi-assessment)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\o\AssessmentController
 * @var $model ommu\akreditasi\models\search\AkreditasiAssessment
 * @var $form yii\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:59 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="akreditasi-assessment-search search-form">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'options' => [
			'data-pjax' => 1
		],
	]); ?>

		<?php echo $form->field($model, 'simulationLibraryId');?>

		<?php echo $form->field($model, 'indicatorQuestionComponent');?>

		<?php echo $form->field($model, 'choice');?>

		<?php echo $form->field($model, 'creation_date')
			->input('date');?>

		<?php echo $form->field($model, 'creationDisplayname');?>

		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']); ?>
			<?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>