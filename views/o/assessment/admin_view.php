<?php
/**
 * Akreditasi Assessments (akreditasi-assessment)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\o\AssessmentController
 * @var $model ommu\akreditasi\models\AkreditasiAssessment
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:59 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    if (isset($model->simulation)) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
        $this->params['breadcrumbs'][] = ['label' => $model->simulation->library->library_name, 'url' => ['library/admin/view', 'id' => $model->simulation->library_id], 'class' => 'modal-btn'];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['admin/manage', 'library' => $model->simulation->library_id]];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation->simulation_start)]), 'url' => ['admin/view', 'id' => $model->simulation_id]];
    }
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Assessment'), 'url' => ['manage', 'simulation' => $model->simulation_id]];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Detail');

    $this->params['menu']['content'] = [
        ['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
    ];
} ?>

<div class="akreditasi-assessment-view">

<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'simulationLibraryId',
		'value' => function ($model) {
			$simulationLibraryId = isset($model->simulation) ? $model->simulation->library->library_name : '-';
            if ($simulationLibraryId != '-') {
				return Html::a($simulationLibraryId, ['simulation/view', 'id' => $model->simulation_id], ['title' => $simulationLibraryId, 'class' => 'modal-btn']);
            }
			return $simulationLibraryId;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'indicator.component_id',
		'value' => function ($model) {
			$componentName = isset($model->indicator) ? $model->indicator->component->component_name : '-';
            if ($componentName != '-') {
				return Html::a($componentName, ['instrument/component/view', 'id' => $model->indicator->component_id], ['title' => $componentName, 'class' => 'modal-btn']);
            }
			return $componentName;
		},
		'format' => 'html',
	],
	[
		'label' => Yii::t('app', 'Indicator Question'),
		'value' => function ($model) {
			return isset($model->indicator) ? $model->indicator->question : '-';
		},
		'format' => 'html',
	],
	[
		'attribute' => 'choice',
		'value' => $model->choice ? $model->choice : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'creation_date',
		'value' => Yii::$app->formatter->asDatetime($model->creation_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'creationDisplayname',
		'value' => isset($model->creation) ? $model->creation->displayname : '-',
		'visible' => !$small,
	],
];

echo DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]); ?>

</div>