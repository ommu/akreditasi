<?php
/**
 * Akreditasi Simulation Histories (akreditasi-simulation-history)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\o\StepController
 * @var $model ommu\akreditasi\models\search\AkreditasiSimulationHistory
 * @var $form yii\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 6 January 2021, 18:55 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="akreditasi-simulation-history-search search-form">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'options' => [
			'data-pjax' => 1
		],
	]); ?>

		<?php echo $form->field($model, 'userDisplayname');?>

		<?php echo $form->field($model, 'step_component_id');?>

		<?php echo $form->field($model, 'step_component_date')
			->input('date');?>

		<?php echo $form->field($model, 'step_component_score');?>

		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']); ?>
			<?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>