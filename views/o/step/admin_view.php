<?php
/**
 * Akreditasi Simulation Histories (akreditasi-simulation-history)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\o\StepController
 * @var $model ommu\akreditasi\models\AkreditasiSimulationHistory
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 6 January 2021, 18:55 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    if (isset($model->simulation)) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
        $this->params['breadcrumbs'][] = ['label' => $model->simulation->library->library_name, 'url' => ['library/admin/view', 'id' => $model->simulation->library_id], 'class' => 'modal-btn'];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['admin/manage', 'library' => $model->simulation->library_id]];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation->simulation_start)]), 'url' => ['admin/view', 'id' => $model->simulation_id]];
    }
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Step'), 'url' => ['o/step/manage', 'simulation' => $model->simulation_id]];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Detail');

    $this->params['menu']['content'] = [
        ['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
    ];
} ?>

<div class="akreditasi-simulation-history-view">

<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'simulation.library_id',
		'value' => function ($model) {
            $libraryName = isset($model->simulation->library) ? $model->simulation->library->library_name : '-';
            if ($libraryName != '-') {
                return Html::a($libraryName, ['library/admin/view', 'id' => $model->simulation->library_id], ['title' => $libraryName, 'class' => 'modal-btn']);
            }
            return $libraryName;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'simulation_id',
		'value' => function ($model) {
            return $model->simulation->parseSimulationDate(false);
		},
		'format' => 'html',
	],
	[
		'attribute' => 'step_component_id',
		'value' => isset($model->component) ? $model->component->component_name : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'step_component_score',
		'value' => $model->step_component_score,
		'visible' => !$small,
	],
	[
		'attribute' => 'step_component_date',
		'value' => Yii::$app->formatter->asDatetime($model->step_component_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'userDisplayname',
		'value' => isset($model->user) ? $model->user->displayname : '-',
		'visible' => !$small,
	],
];

echo DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]); ?>

</div>