<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\search\AkreditasiSimulation
 * @var $form yii\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="akreditasi-simulation-search search-form">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'options' => [
			'data-pjax' => 1
		],
	]); ?>

		<?php echo $form->field($model, 'libraryName');?>

		<?php echo $form->field($model, 'userDisplayname');?>

		<?php echo $form->field($model, 'simulation_start')
			->input('date');?>

		<?php echo $form->field($model, 'simulation_end')
			->input('date');?>

        <?php echo $form->field($model, 'simulation_score');?>

        <?php echo $form->field($model, 'simulation_document');?>

        <?php echo $form->field($model, 'generated_date');?>

		<?php echo $form->field($model, 'accreditation')
			->dropDownList($model::getAccreditations('small'), ['prompt' => '']);?>

        <?php echo $form->field($model, 'history')
            ->dropDownList($model->filterYesNo(), ['prompt' => '']);?>

        <?php echo $form->field($model, 'completed')
            ->dropDownList($model->filterYesNo(), ['completed' => '']);?>

        <?php echo $form->field($model, 'document')
            ->dropDownList($model->filterYesNo(), ['document' => '']);?>

		<?php echo $form->field($model, 'creation_date')
			->input('date');?>

		<?php echo $form->field($model, 'modified_date')
			->input('date');?>

		<?php echo $form->field($model, 'modifiedDisplayname');?>

		<?php echo $form->field($model, 'updated_date')
			->input('date');?>

		<?php echo $form->field($model, 'publish')
			->dropDownList($model->filterYesNo(), ['prompt' => '']);?>

		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']); ?>
			<?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>