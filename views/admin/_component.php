<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 23 December 2020, 11:14 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php 
$subComponen = $component->getSubComponents('count') ? true : false;
if ($subComponen) {
    $j = 0;
    foreach ($component->subComponents as $sub) {
        $j++;?>
        <h5 <?php echo $j == 1 ? 'class="mt-2"' : '';?>><?php echo join('.', [$i, $j]).'. '.$sub->component_name;?></h5>
        <hr class="border"/>

        <?php echo $this->render('_component', [
            'i' => join('.', [$i, $j]),
            'component' => $sub,
            'form' => $form,
            'assessment' => $assessment,
        ]);?>
<?php }

} else {
    $indicators = $component->indicators;
    $j = 0;
    foreach ($indicators as $indicator) {
        $j++;
        echo $this->render('_indicator', [
            'i' => join('.', [$i, $j]),
            'indicator' => $indicator,
            'form' => $form,
            'assessment' => $assessment,
        ]);
        echo '<hr class="mt-6 mb-6"/>';
    }
}?>