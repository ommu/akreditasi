<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 23 December 2020, 11:14 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$indicatorId = $indicator->id;
echo $form->field($assessment, 'choice['.$indicatorId.']', ['template' => '{beginWrapper}{hint}{input}{error}{endWrapper}', 'horizontalCssClasses' => ['wrapper' => 'col-lg-9 col-md-12 pl-0', 'hint' => 'main-fontsizecolor text-reset']])
    ->radioList($indicator->answer, [
        'item' => function($index, $label, $name, $checked, $value) {
            $checked = $checked ? 'checked' : '';
            $choice = strtoupper($value);
            return "<div class=\"radio\"><label><input type=\"radio\" name=\"{$name}\" value=\"{$value}\" {$checked}> <span class=\"font-weight-bold\">{$choice}.</span> {$label}</label></div>";
        }
    ])
    ->label($assessment->getAttributeLabel('choice'))
    ->hint($i.'. '.$indicator->question);

?>