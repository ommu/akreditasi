<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
if ($simulationLibrary) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
    $this->params['breadcrumbs'][] = ['label' => $model->library->library_name, 'url' => ['library/admin/view', 'id' => $model->library_id], 'class' => 'modal-btn'];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['manage', 'library' => $model->library_id]];
    if ($simulationLibraryCategory) {
        if (!$start) {
            $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation_start)]), 'url' => ['view', 'id' => $model->id]];
            if ($simulation) {
                $this->params['breadcrumbs'][] = Yii::t('app', 'Run');
            }
        } else {
            $this->params['breadcrumbs'][] = Yii::t('app', 'Start');
        }
    }

} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['index']];
}

if ($simulation) {
    \themes\gentelella\assets\WizardAsset::register($this);

    if ($simulationLibrary) {
        echo $this->renderWidget('/library/admin/admin_view', [
            'title' => '',
            'model' => $model->library, 
            'small' => true,
        ]);
    }

    echo $this->renderWizard('_assessment', [
        'navigation' => $navigation,
        'current' => $current,
        'step' => $step,
        'model' => $model,
        'components' => $components,
        'assessment' => $assessment,
    ]);

} else {
    $this->params['breadcrumbs'][] = Yii::t('app', 'Start');

    if ($simulationLibrary) {
        echo $this->renderWidget('/library/admin/admin_view', [
            'title' => '',
            'model' => $model->library, 
            'small' => true,
        ]);
    }
    
    echo $this->renderWidget('_alert_desc', [
        'model' => $model, 
        'simulationLibrary' => $simulationLibrary, 
        'simulationLibraryCategory' => $simulationLibraryCategory,
    ]);
}?>