<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\components\widgets\ActiveForm;
use ommu\selectize\Selectize;

if (!empty($librarian['headDepartment'])) {
    $model->librarian['headDepartment'] = array_key_first($librarian['headDepartment']);
}
if (!empty($librarian['headLibrary'])) {
    $model->librarian['headLibrary'] = array_key_first($librarian['headLibrary']);
}
if (!empty($librarian['librarian'])) {
    $model->librarian['librarian'] = implode(',', $librarian['librarian']);
}

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    if (isset($model->library)) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
        $this->params['breadcrumbs'][] = ['label' => $model->library->library_name, 'url' => ['library/admin/view', 'id' => $model->library_id], 'class' => 'modal-btn'];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['manage', 'library' => $model->library_id]];
    } else {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['index']];
    }
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation_start)]), 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Document');

    $this->params['menu']['content'] = [
        ['label' => Yii::t('app', 'Back to Detail'), 'url' => Url::to(['view', 'id' => $model->id]), 'icon' => 'reply', 'htmlOptions' => ['class' => 'btn btn-default']],
    ];
} ?>

<div class="akreditasi-simulation-document">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal form-label-left'],
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        //'enableClientScript' => true,
        'fieldConfig' => [
            'errorOptions' => [
                'encode' => false,
            ],
        ],
    ]); ?>

    <?php //echo $form->errorSummary($model);?>

    <?php echo $this->description && Yii::$app->request->isAjax ? Html::tag('p', $this->description, ['class' => 'mb-4']) : '';?>

    <?php 
    $template = '{label}{beginWrapper}<h5>'.Yii::t('app', 'Document not available.').'</h5>'.Yii::t('app', 'Are you sure you want to generated document simulation?').'{endWrapper}';
    if ($model->document) {
        $simulationDocument = $model::parseDocument($model->simulation_document, $model->library_id, $model->id);
        $template = '{label}{beginWrapper}'.$simulationDocument.'{endWrapper}';
    }
    echo $form->field($model, 'simulation_document', ['template' => $template])
        ->textInput()
        ->label($model->getAttributeLabel('simulation_document')); ?>

    <hr/>

    <?php echo $form->field($model, 'librarian[headDepartment]')
        ->widget(Selectize::className(), [
            'options' => [
                'placeholder' => Yii::t('app', 'Select a {librarian}..', ['librarian' => strtolower($model->getAttributeLabel('librarian[headDepartment]'))]),
            ],
            'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a {librarian}..', ['librarian' => strtolower($model->getAttributeLabel('librarian[headDepartment]'))])], $librarian['headDepartment']),
            'pluginOptions' => [
                'valueField' => 'label',
                'labelField' => 'label',
                'searchField' => ['label'],
                'persist' => false,
                'createOnBlur' => false,
                'create' => true,
            ],
        ])
        ->label($model->getAttributeLabel('librarian[headDepartment]'));
        
    echo $form->field($model, 'librarian[headLibrary]')
        ->widget(Selectize::className(), [
            'options' => [
                'placeholder' => Yii::t('app', 'Select a {librarian}..', ['librarian' => strtolower($model->getAttributeLabel('librarian[headLibrary]'))]),
            ],
            'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a {librarian}..', ['librarian' => strtolower($model->getAttributeLabel('librarian[headDepartment]'))])], $librarian['headLibrary']),
            'pluginOptions' => [
                'valueField' => 'label',
                'labelField' => 'label',
                'searchField' => ['label'],
                'persist' => false,
                'createOnBlur' => false,
                'create' => true,
            ],
        ])
        ->label($model->getAttributeLabel('librarian[headLibrary]'));
        
    echo $form->field($model, 'librarian[librarian]')
        ->widget(Selectize::className(), [
            'options' => [
                'placeholder' => Yii::t('app', 'Select a {librarian}..', ['librarian' => strtolower($model->getAttributeLabel('librarian[librarian]'))]),
            ],
            'pluginOptions' => [
                'plugins' => ['remove_button'],
                'valueField' => 'label',
                'labelField' => 'label',
                'searchField' => ['label'],
                'persist' => false,
                'createOnBlur' => false,
                'create' => true,
            ],
        ])
        ->label($model->getAttributeLabel('librarian[librarian]'));?>

    <?php if ($model->document) { ?>
        <hr/>

        <?php echo $form->field($model, 'regenerate', ['template' => '{input}{error}{hint}{endWrapper}'])
            ->checkbox()
            ->label($model->getAttributeLabel('regenerate'));
            // ->hint(Yii::t('app', 'Are you sure you want to regenerated document simulation?'));
    } ?>

    <?php if (!$model->getErrors()) {
        $model->generated_date = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
    }
    echo $form->field($model, 'generated_date', ['template' => '{input}', 'options' => ['tag' => null]])->hiddenInput(); ?>

    <hr/>

    <?php echo $form->field($model, 'submitButton')
        ->submitButton(['button' => Html::submitButton(Yii::t('app', 'Generate Document'), ['class' => 'btn btn-primary'])]); ?>

    <?php ActiveForm::end(); ?>

</div>