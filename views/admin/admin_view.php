<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    if (isset($model->library)) {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
        $this->params['breadcrumbs'][] = ['label' => $model->library->library_name, 'url' => ['library/admin/view', 'id' => $model->library_id], 'class' => 'modal-btn'];
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['manage', 'library' => $model->library_id]];
    } else {
        $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['index']];
    }
    $this->params['breadcrumbs'][] = Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation_start)]);

    $this->params['menu']['content'] = [
        ['label' => Yii::t('app', 'Update'), 'url' => Url::to(['update', 'id' => $model->id]), 'icon' => 'pencil', 'htmlOptions' => ['class' => 'btn btn-primary']],
        ['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
    ];
    if ($model->completed == 1) {
        $this->params['menu']['content'] = ArrayHelper::merge($this->params['menu']['content'], [
            ['label' => Yii::t('app', 'Document'), 'url' => Url::to(['document', 'id' => $model->id]), 'icon' => 'file', 'htmlOptions' => ['title' => Yii::t('app', 'Document'), 'class' => 'btn btn-success']],
        ]);
    }
} ?>

<div class="akreditasi-simulation-view">

<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'publish',
		'value' => $model->quickAction(Url::to(['publish', 'id' => $model->primaryKey]), $model->publish),
		'format' => 'raw',
		'visible' => !$small,
	],
	[
		'attribute' => 'libraryName',
		'value' => function ($model) {
			$libraryName = isset($model->library) ? $model->library->library_name : '-';
            if ($libraryName != '-') {
				return Html::a($libraryName, ['library/admin/view', 'id' => $model->library_id], ['title' => $libraryName, 'class' => 'modal-btn']);
            }
			return $libraryName;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'simulation_start',
		'value' => $this->asDatetime($model->simulation_start),
	],
	[
		'attribute' => 'simulation_end',
		'value' => Yii::$app->formatter->asDatetime($model->simulation_end, 'medium'),
	],
	[
		'attribute' => 'userDisplayname',
		'value' => isset($model->user) ? $model->user->displayname : '-',
	],
	[
		'attribute' => 'simulation_score',
		'value' => function ($model) {
            if ($model->completed == 1) {
                return $model->simulation_score.' / '.$model::getAccreditations('large', $model->accreditation);
            }
            return '-';
		},
		'format' => 'html',
	],
	[
		'attribute' => 'step_component_id',
		'value' => function ($model) {
            return $model->parseSimulationStep();
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'assessments',
		'value' => function ($model) {
			$assessments = $model->getAssessments(true);
			return Html::a($assessments, ['o/assessment/manage', 'simulation' => $model->primaryKey], ['title' => Yii::t('app', '{count} assessments', ['count' => $assessments])]);
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'creation_date',
		'value' => Yii::$app->formatter->asDatetime($model->creation_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'modified_date',
		'value' => Yii::$app->formatter->asDatetime($model->modified_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'modifiedDisplayname',
		'value' => isset($model->modified) ? $model->modified->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'updated_date',
		'value' => Yii::$app->formatter->asDatetime($model->updated_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => '',
		'value' => function ($model) {
            $button = Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Update'), 'class' => 'btn btn-primary btn-sm modal-btn']);
            if ($model->completed == 1) {
                return $button.Html::a('<i class="fa fa-file"></i> '.Yii::t('app', 'Document'), ['document', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Document'), 'class' => 'btn btn-success btn-sm']);
            }
            return $button;
		},
		'format' => 'html',
		'visible' => !$small && Yii::$app->request->isAjax ? true : false,
	],
];

echo DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]); ?>

</div>
