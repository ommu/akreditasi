<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 23 December 2020, 11:14 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="jumbotron mb-0">
    <h1><?php echo !$simulationLibrary ? 
        Yii::t('app', 'Simulation cannot be run') : 
        Yii::t('app', 'This library category not support') ;?></h1>
    <p><?php echo !$simulationLibrary ? 
        Yii::t('app', 'Please update the simulation data: {update}', [
            'update' => Html::a('<i class="fa fa-pencil"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Update Simulation'), 'class' => 'btn btn-warning btn-xs modal-btn']),
        ]) : 
        '' ;?></p>
    <?php echo $simulationLibrary ? 
        Html::a('<i class="fa fa-reply"></i> '.Yii::t('app', 'Back to library simulation'), Url::to(['manage', 'library' => $model->library_id]), ['class' => 'btn btn-default']) : 
        ''; ?>
</div>