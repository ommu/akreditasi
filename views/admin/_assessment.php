<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 23 December 2020, 11:14 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
?>

<?php if ($components) {
    $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal form-label-left'],
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        //'enableClientScript' => true,
        'fieldConfig' => [
            'errorOptions' => [
                'encode' => false,
            ],
        ],
    ]);

    echo $form->field($assessment, 'simulation_id', ['template' => '{input}', 'options' => ['tag' => null]])->hiddenInput();
    echo $form->field($assessment, 'componentId', ['template' => '{input}', 'options' => ['tag' => null]])->hiddenInput();

    $i = 0;
    foreach ($components as $val) {
        $i++; ?>
        <div class="x_title border border-width-3 border-top-0 border-right-0 border-left-0 <?php echo $i != 1 ? 'mt-8' : '';?>">
            <h2><?php echo join('.', [$step, $i]).'. '.$val->component_name;?></h2>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <?php echo $this->render('_component', [
                'i' => join('.', [$step, $i]),
                'component' => $val,
                'form' => $form,
                'assessment' => $assessment,
            ]);?>
            <div class="clearfix"></div>
        </div>

<?php }

    echo $form->field($model, 'submitButton', ['horizontalCssClasses' => ['wrapper' => 'col-xs-12 text-right']])
        ->submitButton(['offset' => false, 'button' => Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary'])]);

    ActiveForm::end();
}?>
