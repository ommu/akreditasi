<?php
/**
 * Akreditasi Simulations (akreditasi-simulation)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
$this->params['breadcrumbs'][] = ['label' => $model->library->library_name, 'url' => ['library/admin/view', 'id' => $model->library_id], 'class' => 'modal-btn'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simulation'), 'url' => ['manage', 'library' => $model->library_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Start: {simulation_start}', ['simulation_start' => $this->asDatetime($model->simulation_start)]), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Complete');
?>

<div class="jumbotron mb-0">
    <h1><?php echo Yii::t('app', 'Simulation has been completed'); ?></h1>
    <p><?php echo Yii::t('app', 'View the simulation data: {view} or {document}', [
            'view' => Html::a('<i class="fa fa-eye"></i> '.Yii::t('app', 'View'), ['view', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Simulation Data'), 'class' => 'btn btn-info']),
            'document' => Html::a('<i class="fa fa-file"></i> '.Yii::t('app', 'Generate Document'), ['document', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Generate Document'), 'class' => 'btn btn-success']),
        ]); ?></p>
    <?php echo Html::a('<i class="fa fa-reply"></i> '.Yii::t('app', 'Back to library simulation'), Url::to(['manage', 'library' => $model->library_id]), ['class' => 'btn btn-default']); ?>
</div>