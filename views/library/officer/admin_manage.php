<?php
/**
 * Akreditasi Officers (akreditasi-officer)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\OfficerController
 * @var $model ommu\akreditasi\models\AkreditasiOfficer
 * @var $searchModel ommu\akreditasi\models\search\AkreditasiOfficer
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\GridView;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
if ($library != null) {
	$this->params['breadcrumbs'][] = ['label' => $library->library_name, 'url' => ['library/admin/view', 'id' => $library->id], 'class' => 'modal-btn'];
}
$this->params['breadcrumbs'][] = $this->title;

$createUrl = Url::to(['create']);
$importUrl = Url::to(['import']);
if ($library != null) {
    $createUrl = Url::to(['create', 'id' => $library->id]);
    $importUrl = Url::to(['import', 'id' => $library->id]);
}
$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Add Librarian'), 'url' => $createUrl, 'icon' => 'plus-square', 'htmlOptions' => ['class' => 'btn btn-success modal-btn']],
	['label' => Yii::t('app', 'Import'), 'url' => $importUrl, 'icon' => 'file-excel-o', 'htmlOptions' => ['class' => 'btn btn-default modal-btn']],
];
$this->params['menu']['option'] = [
	//['label' => Yii::t('app', 'Search'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Grid Option'), 'url' => 'javascript:void(0);'],
];
?>

<div class="akreditasi-officer-manage">
<?php Pjax::begin(); ?>

<?php if ($library != null) {
    echo $this->render('/library/admin/admin_view', ['model' => $library, 'small' => true]);
} ?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php echo $this->render('_option_form', ['model' => $searchModel, 'gridColumns' => $searchModel->activeDefaultColumns($columns), 'route' => $this->context->route]); ?>

<?php
$columnData = $columns;
array_push($columnData, [
	'class' => 'app\components\grid\ActionColumn',
	'header' => Yii::t('app', 'Option'),
	'urlCreator' => function($action, $model, $key, $index) {
        if ($action == 'view') {
            return Url::to(['view', 'id' => $key]);
        }
        if ($action == 'update') {
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'delete') {
            return Url::to(['delete', 'id' => $key]);
        }
	},
	'buttons' => [
		'view' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Librarian'), 'class' => 'modal-btn']);
		},
		'update' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Librarian'), 'class' => 'modal-btn']);
		},
		'delete' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				'title' => Yii::t('app', 'Delete Librarian'),
				'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'data-method'  => 'post',
			]);
		},
	],
	'template' => '{view} {update} {delete}',
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columnData,
]); ?>

<?php Pjax::end(); ?>
</div>