<?php
/**
 * Akreditasi Officers (akreditasi-officer)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\OfficerController
 * @var $model ommu\akreditasi\models\AkreditasiOfficer
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use ommu\selectize\Selectize;

$js = <<<JS
	var options = '';
	var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
		'(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
JS;
	$this->registerJs($js, \yii\web\View::POS_END);
?>

<div class="akreditasi-officer-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php
$librarySuggestUrl = Url::to(['library/admin/suggest']);
$libraryOptions = [
	'valueField' => 'id',
	'labelField' => 'name',
	'searchField' => ['name', 'city'],
	'maxItems' => '1',
	'persist' => false,
	'render' => [
		'item' => new JsExpression('function(item, escape) {
			return \'<div>\' +
				(item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
				(item.city ? \'<span class="email">\' + escape(item.city) + \'</span>\' : \'\') +
			\'</div>\';
		}'),
		'option' => new JsExpression('function(item, escape) {
			var label = item.name || item.city;
			var caption = item.name ? item.city : null;
			return \'<div>\' +
				\'<span class="label">\' + escape(label) + \'</span>\' +
				(caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
			\'</div>\';
		}'),
	],
	'onDelete' => new JsExpression('function(value) {
		library_id.clear();
		library_id.clearOptions();
	}'),
];
if ($model->library_id && isset($model->library)) {
	$libraryOptions = ArrayHelper::merge($libraryOptions, [
		'options' => [[
			'id' => $model->library_id,
			'name' => $model->library->library_name,
			'city' => $model->library->city->city_name,
		]]
	]);
}
echo $form->field($model, 'library_id')
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a library...'),
			'class' => 'form-control contacts',
		],
		'url' => $librarySuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => $libraryOptions,
	])
	->label($model->getAttributeLabel('library_id')); ?>

<?php 
$userSuggestUrl = Url::to(['/users/member/suggest']);
$userOptions = [
	'valueField' => 'id',
	'labelField' => 'name',
	'searchField' => ['name', 'email'],
	'maxItems' => '1',
	'persist' => false,
	'render' => [
		'item' => new JsExpression('function(item, escape) {
			return \'<div>\' +
				(item.name ? \'<span class="name">\' + escape(item.name) + \'</span>\' : \'\') +
				(item.email ? \'<span class="email">\' + escape(item.email) + \'</span>\' : \'\') +
			\'</div>\';
		}'),
		'option' => new JsExpression('function(item, escape) {
			var label = item.name || item.email;
			var caption = item.name ? item.email : null;
			return \'<div>\' +
				\'<span class="label">\' + escape(label) + \'</span>\' +
				(caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
			\'</div>\';
		}'),
	],
	'createFilter' => new JsExpression('function(input) {
		var match, regex;

		regex = new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\');
		match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[0]);

		regex = new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\');
		match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[2]);

		return false;
	}'),
	'create' => new JsExpression('function(input) {
        if ((new RegExp(\'^\' + REGEX_EMAIL + \'$\', \'i\')).test(input)) {
			return {email: input};
		}
		var match = input.match(new RegExp(\'^([^<]*)\<\' + REGEX_EMAIL + \'\>$\', \'i\'));
        if (match) {
			return {
				email : match[2],
				name  : $.trim(match[1])
			};
		}
		alert(\'Invalid email address.\');
		return false;
	}'),
	// 'onChange' => new JsExpression('function(value) {
	// 	options = this.options;
	// 	var userSelected = this.options[value];
	// 	$(\'form\').find(\'#speaker_name\').val(userSelected.name);
	// }'),
	'onDelete' => new JsExpression('function(value) {
		user_id.clear();
		user_id.clearOptions();
	}'),
];
if ($model->user_id && isset($model->user)) {
	$userOptions = ArrayHelper::merge($userOptions, [
		'options' => [[
			'id' => $model->user_id,
			'email' => $model->user->email,
			'name' => $model->user->displayname,
			'photo' => $model->user->photos,
		]]
	]);
}
echo $form->field($model, 'user_id')
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Pick some people...'),
			'class' => 'form-control contacts',
		],
		'url' => $userSuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => $userOptions,
	])
	->label($model->getAttributeLabel('user_id')); ?>

<?php $officerLevel = $model::getOfficerLevel();
echo $form->field($model, 'officer_level')
	->dropDownList($officerLevel, ['prompt' => ''])
	->label($model->getAttributeLabel('officer_level')); ?>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>