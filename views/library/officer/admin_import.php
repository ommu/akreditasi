<?php
/**
 * Akreditasi Officers (akreditasi-officer)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\OfficerController
 * @var $model ommu\akreditasi\models\AkreditasiOfficer
 * @var $searchModel ommu\akreditasi\models\search\AkreditasiOfficer
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 26 February 2021, 6:37 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$importAsset = \ommu\akreditasi\components\assets\TemplateImportAsset::register($this);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['index']];
if ($model != null) {
    $this->params['breadcrumbs'][] = ['label' => $model->library_name, 'url' => ['library/admin/view', 'id' => $model->id], 'class' => 'modal-btn'];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Librarian'), 'url' => ['manage', 'library' => $model->id]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Librarian'), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = Yii::t('app', 'Import');

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Back to Librarians'), 'url' => Url::to(['manage', 'library' => $model->id]), 'icon' => 'reply', 'htmlOptions' => ['class' => 'btn btn-default']],
];
?>

<div class="kckr-media-import">

<?php $formAttr = [
	'class' => 'form-horizontal form-label-left',
	'enctype' => 'multipart/form-data',
];
if (Yii::$app->request->isAjax) {
    $formAttr = ArrayHelper::merge($formAttr, [
        'onpost' => 'onpost',
    ]);
}
echo Html::beginForm(Yii::$app->request->absoluteUrl, 'post', $formAttr); ?>

<?php echo $this->description && Yii::$app->request->isAjax ? Html::tag('p', $this->description, ['class' => 'mb-4']) : '';?>

<div class="form-group row">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="importLibrarian"><?php echo Yii::t('app', 'File');?></label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<?php echo Html::fileInput('importLibrarian', '', ['id' => 'importLibrarian']);?>
		<div class="help-block help-block-error">
			<?php echo Yii::t('app', 'extensions are allowed: {extensions}', ['extensions' => $importFileType]);?>
            <hr/>
            <?php echo Html::a('<i class="fa fa-file-excel-o"></i> '.Yii::t('app', 'Import Template'), $importAsset->baseUrl.'/librarian_import_template.xlsx', ['title' => Yii::t('app', 'Import Template'), 'class' => 'btn btn-info btn-sm']);?>
		</div>
	</div>
</div>

<hr/>

<div class="form-group row">
	<div class="col-md-9 col-sm-9 col-xs-12 col-sm-offset-3">
		<?php echo Html::submitButton(Yii::t('app', 'Import'), ['class' => 'btn btn-success']);?>
	</div>
</div>

<?php echo Html::endForm(); ?>

</div>