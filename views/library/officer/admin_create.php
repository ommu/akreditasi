<?php
/**
 * Akreditasi Officers (akreditasi-officer)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\OfficerController
 * @var $model ommu\akreditasi\models\AkreditasiOfficer
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['library/admin/index']];
if (isset($model->library)) {
    $this->params['breadcrumbs'][] = ['label' => $model->library->library_name, 'url' => ['library/admin/view', 'id' => $model->library_id], 'class' => 'modal-btn'];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Librarian'), 'url' => ['manage', 'library' => $model->library_id]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Librarian'), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>

<div class="akreditasi-officer-create">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>
