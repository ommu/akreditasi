<?php
/**
 * Akreditasi Libraries (akreditasi-library)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use ommu\akreditasi\models\AkreditasiSimulation;

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $model->library_name;
} ?>

<div class="akreditasi-library-view">

<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'publish',
		'value' => $model->quickAction(Url::to(['publish', 'id' => $model->primaryKey]), $model->publish),
		'format' => 'raw',
		'visible' => !$small,
	],
	[
		'attribute' => 'categoryName',
		'value' => function ($model) {
			$categoryName = isset($model->category) ? $model->category->category_name : '-';
            if ($categoryName != '-') {
				return Html::a($categoryName, ['setting/category/view', 'id' => $model->cat_id], ['title' => $categoryName, 'class' => 'modal-btn']);
            }
			return $categoryName;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'library_name',
		'value' => $model->library_name ? $model->library_name : '-',
	],
	[
		'attribute' => 'npp',
		'value' => $model->npp ? $model->npp : '-',
	],
	[
		'attribute' => 'accreditation',
		'value' => function ($model) {
            return AkreditasiSimulation::getAccreditations('large', $model->accreditation);
        },
	],
	[
		'attribute' => 'library_address',
		'value' => function ($model) {
            return $this::parseAddress($model->library_address);
		},
		'visible' => !$small,
	],
	[
		'attribute' => 'library_contact',
		'value' => function ($model) {
            return $this::parseContact($model->library_contact);
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'library_city',
		'value' => $model->city ? $model->city->city_name : '-',
	],
	[
		'attribute' => 'officer',
		'value' => function ($model) {
			$officers = $model->getOfficers(true);
			return Html::a($officers, ['library/officer/manage', 'library' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} officers', ['count' => $officers])]);
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'simulation',
		'value' => function ($model) {
			$simulations = $model->getSimulations(true);
			return Html::a($simulations, ['admin/manage', 'library' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} simulations', ['count' => $simulations])]);
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'creation_date',
		'value' => Yii::$app->formatter->asDatetime($model->creation_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'creationDisplayname',
		'value' => isset($model->creation) ? $model->creation->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'modified_date',
		'value' => Yii::$app->formatter->asDatetime($model->modified_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'modifiedDisplayname',
		'value' => isset($model->modified) ? $model->modified->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'updated_date',
		'value' => Yii::$app->formatter->asDatetime($model->updated_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => '',
		'value' => Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Update'), 'class' => 'btn btn-primary btn-sm modal-btn']),
		'format' => 'html',
		'visible' => !$small && Yii::$app->request->isAjax ? true : false,
	],
];

echo DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]); ?>

</div>