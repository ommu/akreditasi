<?php
/**
 * Akreditasi Libraries (akreditasi-library)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 24 February 2021, 17:37 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$importAsset = \ommu\akreditasi\components\assets\TemplateImportAsset::register($this);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Import');

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Back to Libraries'), 'url' => Url::to(['manage']), 'icon' => 'reply', 'htmlOptions' => ['class' => 'btn btn-default']],
];
?>

<div class="kckr-media-import">

<?php $formAttr = [
	'class' => 'form-horizontal form-label-left',
	'enctype' => 'multipart/form-data',
];
if (Yii::$app->request->isAjax) {
    $formAttr = ArrayHelper::merge($formAttr, [
        'onpost' => 'onpost',
    ]);
}
echo Html::beginForm(Yii::$app->request->absoluteUrl, 'post', $formAttr); ?>

<?php echo $this->description && Yii::$app->request->isAjax ? Html::tag('p', $this->description, ['class' => 'mb-4']) : '';?>

<div class="form-group row">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="importLibrary"><?php echo Yii::t('app', 'File');?></label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<?php echo Html::fileInput('importLibrary', '', ['id' => 'importLibrary']);?>
		<div class="help-block help-block-error">
			<?php echo Yii::t('app', 'extensions are allowed: {extensions}', ['extensions' => $importFileType]);?>
            <hr/>
            <?php echo Html::a('<i class="fa fa-file-excel-o"></i> '.Yii::t('app', 'Import Template'), $importAsset->baseUrl.'/library_import_template.xlsx', ['title' => Yii::t('app', 'Import Template'), 'class' => 'btn btn-info btn-sm']);?>
		</div>
	</div>
</div>

<hr/>

<div class="form-group row">
	<div class="col-md-9 col-sm-9 col-xs-12 col-sm-offset-3">
		<?php echo Html::submitButton(Yii::t('app', 'Import'), ['class' => 'btn btn-success']);?>
	</div>
</div>

<?php echo Html::endForm(); ?>

</div>