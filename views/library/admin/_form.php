<?php
/**
 * Akreditasi Libraries (akreditasi-library)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\components\widgets\ActiveForm;
use ommu\akreditasi\models\AkreditasiCategory;
use ommu\akreditasi\models\AkreditasiSimulation;
use ommu\selectize\Selectize;
use yii\web\JsExpression;
use ommu\core\models\CoreZoneCity;
use ommu\core\models\CoreZoneProvince;
use ommu\core\models\CoreZoneCountry;

$country = $model->library_address['country'];
$province = $model->library_address['province'];
$city = $model->library_address['city'];
$district = $model->library_address['district'];
$village = $model->library_address['village'];

$js = <<<JS
	var country, province, city, district, village;
	var v_country = '$country';
	var v_province = '$province';
	var v_city = '$city';
	var v_district = '$district';
	var v_village = '$village';
JS;
	$this->registerJs($js, \yii\web\View::POS_END);
?>

<div class="akreditasi-library-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php $category = AkreditasiCategory::getCategory();
echo $form->field($model, 'cat_id')
	->dropDownList($category, ['prompt' => ''])
	->label($model->getAttributeLabel('cat_id')); ?>

<?php echo $form->field($model, 'library_name')
	->textInput()
	->label($model->getAttributeLabel('library_name')); ?>

<?php echo $form->field($model, 'npp')
	->textInput(['maxlength' => true])
	->label($model->getAttributeLabel('npp')); ?>

<?php $accreditation = AkreditasiSimulation::getAccreditations('large');
echo $form->field($model, 'accreditation')
	->dropDownList($accreditation, ['prompt' => ''])
    ->label($model->getAttributeLabel('accreditation')); ?>

<hr/>

<?php
$villageSuggestUrl = Url::to(['/admin/zone/village/suggest', 'extend' => 'village_name,zipcode,district_name,city_id,province_id,country_id']);
$districtSuggestUrl = Url::to(['/admin/zone/district/suggest', 'extend' => 'district_name,city_id,province_id,country_id']);
$citySuggestUrl = Url::to(['/admin/zone/city/suggest', 'extend' => 'city_name,province_id,country_id']);
$provinceSuggestUrl = Url::to(['/admin/zone/province/suggest', 'extend' => 'country_id']);
$countrySuggestUrl = Url::to(['/admin/zone/country/suggest']);

$villageClass = (Yii::$app->request->isAjax || (!Yii::$app->request->isAjax && !$model->isNewRecord)) ? 'col-sm-4 col-xs-6 col-sm-offset-3 mt-3' : 'col-md-3 col-sm-4 col-xs-6 col-sm-offset-3 mt-3';
$officeAddressVillage = $form->field($model, 'library_address[village]', ['template' => '{beginWrapper}{input}{endWrapper}', 'horizontalCssClasses' => ['wrapper' => $villageClass], 'options' => ['tag' => null]])
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a village..'),
			'class' => 'form-control contacts',
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a village..')], []),
		'url' => $villageSuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'village_name',
			'labelField' => 'label',
			'searchField' => ['label'],
			'searchConjunction' => 'or',
			'sortField' => ['label'],
			'render' => [
				'item' => new JsExpression('function(item, escape) {
					return \'<div class="item">\' + escape(item.village_name) + \'</div>\';
				}'),
				'option' => new JsExpression('function(item, escape) {
					var label = item.village_name || item.label;
					var caption = item.village_name ? item.label : null;
					return \'<div>\' +
						\'<span class="label">\' + escape(label) + \'</span>\' +
						(caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
					\'</div>\';
				}'),
			],
			'onChange' => new JsExpression('function(value) {
                v_village = value;
                if (!value.length) return;
                var options = this.options;
                var selected = this.options[value];
                if (selected.zipcode) {
                    $(\'form\').find(\'#library_address-zipcode\').val(selected.zipcode);
                }
                if (selected.district_name) {
                    library_address_district.addOption({district_name: selected.district_name, label: selected.district_name});
                    library_address_district.setValue(selected.district_name);
                }
                if (selected.city_id) {
                    library_address_city.setValue(selected.city_id);
                    $(\'form\').find(\'#library_city\').val(selected.city_id);
                }
                if (selected.province_id) {
                    library_address_province.setValue(selected.province_id);
                }
                if (selected.country_id) {
                    library_address_country.setValue(selected.country_id);
                }
			}'),
		],
	])
	->label($model->getAttributeLabel('library_address[village]'));
if ($model->library_address['village']) {
$js = <<<JS
	library_address_village.addOption({label: '{$model->library_address['village']}', village_name: '{$model->library_address['village']}'});
	library_address_village.setValue('{$model->library_address['village']}');
JS;
	$this->registerJs($js, \yii\web\View::POS_END);
} ?>

<?php 
$districtClass = (Yii::$app->request->isAjax || (!Yii::$app->request->isAjax && !$model->isNewRecord)) ? 'col-sm-5 col-xs-6 mt-3' : 'col-md-3 col-sm-5 col-xs-6 mt-3';
$officeAddressDistrict = $form->field($model, 'library_address[district]', ['template' => '{beginWrapper}{input}{endWrapper}', 'horizontalCssClasses' => ['wrapper' => $districtClass], 'options' => ['tag' => null]])
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a district..'),
			'class' => 'form-control contacts',
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a district..')], []),
		'url' => $districtSuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'district_name',
			'labelField' => 'label',
			'searchField' => ['label'],
			'searchConjunction' => 'or',
			'sortField' => ['label'],
			'render' => [
				'item' => new JsExpression('function(item, escape) {
					return \'<div class="item">\' + escape(item.district_name) + \'</div>\';
				}'),
				'option' => new JsExpression('function(item, escape) {
					var label = item.district_name || item.label;
					var caption = item.district_name ? item.label : null;
					return \'<div>\' +
						\'<span class="label">\' + escape(label) + \'</span>\' +
						(caption ? \'<span class="caption">\' + escape(caption) + \'</span>\' : \'\') +
					\'</div>\';
				}'),
			],
			'onChange' => new JsExpression('function(value) {
                v_district = value;
                if (!value.length) return;
                var options = this.options;
                var selected = this.options[value];
                if (selected.city_id) {
                    library_address_city.setValue(selected.city_id);
                    $(\'form\').find(\'#library_city\').val(selected.city_id);
                }
                if (selected.province_id) {
                    library_address_province.setValue(selected.province_id);
                }
                if (selected.country_id) {
                    library_address_country.setValue(selected.country_id);
                }
                // library_address_village.disable(); 
                // library_address_village.clearOptions();
                // library_address_village.load(function(callback) {
                // 	district && district.abort();
                // 	district = $.ajax({
                // 		url: \''.$villageSuggestUrl.'\',
                // 		data: {\'did\': value},
                // 		success: function(results) {
                // 			library_address_village.removeOption(v_village);
                // 			library_address_village.showInput();
                // 			library_address_village.enable();
                // 			callback(results);
                // 		},
                // 		error: function() {
                // 			callback();
                // 		}
                // 	})
                // });
			}'),
		],
	])
	->label($model->getAttributeLabel('library_address[district]'));
if ($model->library_address['district']) {
$js = <<<JS
	library_address_district.addOption({label: '{$model->library_address['district']}', district_name: '{$model->library_address['district']}'});
	library_address_district.setValue('{$model->library_address['district']}');
JS;
	$this->registerJs($js, \yii\web\View::POS_END);
} ?>

<?php echo $form->field($model, 'library_address[place]', ['template' => '{label}{beginWrapper}{input}{endWrapper}'.$officeAddressVillage.$officeAddressDistrict.'{error}{hint}', 'horizontalCssClasses' => ['error' => 'col-sm-6 col-xs-12 col-sm-offset-3', 'hint' => 'col-sm-6 col-xs-12 col-sm-offset-3']])
	->textarea(['rows' => 3, 'cols' => 50, 'maxlength' => 64, 'placeholder' => $model->getAttributeLabel('library_address[place]')])
	->label($model->getAttributeLabel('library_address'))
	->hint(Yii::t('app', 'The number, street, district and village of the postal address for this libraries')); ?>

<?php echo $form->field($model, 'library_address[city]')
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a city..'),
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a city..')], CoreZoneCity::getCity()),
		// 'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a city..')], []),
		// 'url' => $citySuggestUrl,
		// 'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'id',
			'labelField' => 'label',
			'searchField' => ['label'],
			'sortField' => ['label'],
			'onChange' => new JsExpression('function(value) {
                v_city = value;
                if (!value.length) return;
                $(\'form\').find(\'#library_city\').val(value);
                var options = this.options;
                var selected = this.options[value];
                if (selected.province_id) {
                    library_address_province.setValue(selected.province_id);
                }
                if (selected.country_id) {
                    library_address_country.setValue(selected.country_id);
                }
                // library_address_district.disable(); 
                // library_address_district.clearOptions();
                // library_address_district.load(function(callback) {
                // 	city && city.abort();
                // 	city = $.ajax({
                // 		url: \''.$districtSuggestUrl.'\',
                // 		data: {\'cid\': value},
                // 		success: function(results) {
                // 			library_address_district.removeOption(v_district);
                // 			library_address_district.showInput();
                // 			library_address_district.enable();
                // 			callback(results);
                // 		},
                // 		error: function() {
                // 			callback();
                // 		}
                // 	})
                // });
			}'),
		],
	])
	->label($model->getAttributeLabel('library_address[city]'))
	->hint(Yii::t('app', 'The city (or locality) line of the postal address for this libraries')); ?>

<?php echo $form->field($model, 'library_address[province]')
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a province..'),
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a province..')], CoreZoneProvince::getProvince()),
		// 'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a province..')], []),
		// 'url' => $provinceSuggestUrl,
		// 'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'id',
			'labelField' => 'label',
			'searchField' => ['label'],
			'sortField' => ['label'],
			'onChange' => new JsExpression('function(value) {
                v_province = value;
                if (!value.length) return;
                var options = this.options;
                var selected = this.options[value];
                if (selected.country_id) {
                    library_address_country.setValue(selected.country_id);
                }
                // library_address_city.disable(); 
                // library_address_city.clearOptions();
                // library_address_city.load(function(callback) {
                // 	province && province.abort();
                // 	province = $.ajax({
                // 		url: \''.$citySuggestUrl.'\',
                // 		data: {\'pid\': value},
                // 		success: function(results) {
                // 			library_address_city.removeOption(v_city);
                // 			library_address_city.showInput();
                // 			library_address_city.enable();
                // 			callback(results);
                // 		},
                // 		error: function() {
                // 			callback();
                // 		}
                // 	})
                // });
			}'),
		],
	])
	->label($model->getAttributeLabel('library_address[province]'))
	->hint(Yii::t('app', 'The province (or region) line of the postal address for this libraries')); ?>

<?php echo $form->field($model, 'library_address[zipcode]')
	->textInput(['maxlength' => 6])
	->label($model->getAttributeLabel('library_address[zipcode]')); ?>

<?php echo $form->field($model, 'library_address[country]')
	->widget(Selectize::className(), [
		'cascade' => true,
		'options' => [
			'placeholder' => Yii::t('app', 'Select a country..'),
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a country..')], CoreZoneCountry::getCountry()),
		// 'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a country..')], []),
		// 'url' => $countrySuggestUrl,
		// 'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'id',
			'labelField' => 'label',
			'searchField' => ['label'],
			'sortField' => ['label'],
			'onChange' => new JsExpression('function(value) {
                v_country = value;
                if (!value.length) return;
                // library_address_province.disable(); 
                // library_address_province.clearOptions();
                // library_address_province.load(function(callback) {
                // 	country && country.abort();
                // 	country = $.ajax({
                // 		url: \''.$provinceSuggestUrl.'\',
                // 		data: {\'cid\': value},
                // 		success: function(results) {
                // 			library_address_province.removeOption(v_province);
                // 			library_address_province.showInput();
                // 			library_address_province.enable();
                // 			callback(results);
                // 		},
                // 		error: function() {
                // 			callback();
                // 		}
                // 	})
                // });
			}'),
		],
	])
	->label($model->getAttributeLabel('library_address[country]')); ?>

<hr/>

<?php echo $form->field($model, 'library_contact[phone]')
	->textInput()
	->label($model->getAttributeLabel('library_contact[phone]'))
	->hint(Yii::t('app', 'A telephone number to contact this libraries')); ?>

<?php echo $form->field($model, 'library_contact[fax]')
	->textInput()
	->label($model->getAttributeLabel('library_contact[fax]'))
	->hint(Yii::t('app', 'A fax number to contact this libraries')); ?>

<?php echo $form->field($model, 'library_contact[hotline]')
	->textarea(['rows' => 3, 'cols' => 50])
	->label($model->getAttributeLabel('library_contact[hotline]')); ?>

<?php echo $form->field($model, 'library_contact[email]')
	->textInput()
	->label($model->getAttributeLabel('library_contact[email]'))
	->hint(Yii::t('app', 'An email address to contact this libraries')); ?>

<?php echo $form->field($model, 'library_contact[website]')
	->textInput()
	->label($model->getAttributeLabel('library_contact[website]'))
	->hint(Yii::t('app', 'A website for this libraries')); ?>

<hr/>

<?php echo $form->field($model, 'library_city', ['template' => '{input}', 'options' => ['tag' => null]])->hiddenInput(); ?>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php $model->backToManage = 1;
echo $form->field($model, 'backToManage')
	->checkbox()
	->label($model->getAttributeLabel('backToManage')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>