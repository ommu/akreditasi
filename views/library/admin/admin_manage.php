<?php
/**
 * Akreditasi Libraries (akreditasi-library)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 * @var $searchModel ommu\akreditasi\models\search\AkreditasiLibrary
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = $this->title;

$queryParams = Yii::$app->request->queryParams;
$importUrl = ['export'];
if (!empty($queryParams)) {
    $importUrl = ArrayHelper::merge($importUrl, $queryParams);
}

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Add Library'), 'url' => Url::to(['create']), 'icon' => 'plus-square', 'htmlOptions' => ['class' => 'btn btn-success modal-btn']],
	['label' => Yii::t('app', 'Import'), 'url' => Url::to(['import']), 'icon' => 'file-excel-o', 'htmlOptions' => ['class' => 'btn btn-default modal-btn']],
	// ['label' => Yii::t('app', 'Export'), 'url' => Url::to($importUrl), 'icon' => 'file-excel-o', 'htmlOptions' => ['class' => 'btn btn-dark modal-btn']],
];
$this->params['menu']['option'] = [
	//['label' => Yii::t('app', 'Search'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Grid Option'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Export'), 'url' => 'javascript:void(0);'],
];
?>

<div class="akreditasi-library-manage">
<?php Pjax::begin(); ?>

<?php if ($category != null) {
    echo $this->render('/setting/category/admin_view', ['model' => $category, 'small' => true]);
} ?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php echo $this->render('_option_form', ['model' => $searchModel, 'gridColumns' => $searchModel->activeDefaultColumns($columns), 'route' => $this->context->route]); ?>

<?php
$columnData = $columns;
array_push($columnData, [
	'class' => 'app\components\grid\ActionColumn',
	'header' => Yii::t('app', 'Option'),
	'urlCreator' => function($action, $model, $key, $index) {
        if ($action == 'view') {
            return Url::to(['view', 'id' => $key]);
        }
        if ($action == 'update') {
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'delete') {
            return Url::to(['delete', 'id' => $key]);
        }
	},
	'buttons' => [
		'view' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Library'), 'class' => 'modal-btn']);
		},
		'update' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Library'), 'class' => 'modal-btn']);
		},
		'delete' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				'title' => Yii::t('app', 'Delete Library'),
				'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'data-method'  => 'post',
			]);
		},
	],
	'template' => '{view} {update} {delete}',
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columnData,
]); ?>

<?php Pjax::end(); ?>
</div>