<?php
/**
 * Akreditasi Libraries (akreditasi-library)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Library'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>

<div class="akreditasi-library-create">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>
