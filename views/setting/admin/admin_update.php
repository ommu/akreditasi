<?php
/**
 * Akreditasi Settings (akreditasi-setting)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\setting\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSetting
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 06 October 2019, 22:16 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

if ($breadcrumb) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    $this->params['breadcrumbs'][] = Yii::t('app', 'Settings');
}

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Reset'), 'url' => Url::to(['delete']), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to reset this setting?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
];
?>

<div class="akreditasi-setting-update">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>