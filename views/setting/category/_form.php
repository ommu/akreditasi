<?php
/**
 * Akreditasi Categories (akreditasi-category)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\setting\CategoryController
 * @var $model ommu\akreditasi\models\AkreditasiCategory
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:06 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use app\components\widgets\ActiveForm;
?>

<div class="akreditasi-category-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'category_name')
	->textInput(['maxlength' => true])
	->label($model->getAttributeLabel('category_name')); ?>

<?php echo $form->field($model, 'category_desc')
	->textarea(['rows' => 4, 'cols' => 50])
	->label($model->getAttributeLabel('category_desc')); ?>

<?php echo $form->field($model, 'order')
	->textInput(['type' => 'number'])
	->label($model->getAttributeLabel('order')); ?>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>