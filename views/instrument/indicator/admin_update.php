<?php
/**
 * Akreditasi Indicators (akreditasi-indicator)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\IndicatorController
 * @var $model ommu\akreditasi\models\AkreditasiIndicator
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 23:01 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instrument'), 'url' => ['instrument/component/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Indicator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->component->component_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Back to Detail'), 'url' => Url::to(['view', 'id' => $model->id]), 'icon' => 'eye', 'htmlOptions' => ['class' => 'btn btn-info']],
	['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
];
?>

<div class="akreditasi-indicator-update">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>