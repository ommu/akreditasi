<?php
/**
 * Akreditasi Indicators (akreditasi-indicator)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\IndicatorController
 * @var $model ommu\akreditasi\models\AkreditasiIndicator
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 23:01 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instrument'), 'url' => ['instrument/component/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Indicator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>

<div class="akreditasi-indicator-create">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>
