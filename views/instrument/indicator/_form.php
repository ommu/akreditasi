<?php
/**
 * Akreditasi Indicators (akreditasi-indicator)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\IndicatorController
 * @var $model ommu\akreditasi\models\AkreditasiIndicator
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 23:01 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\widgets\ActiveForm;
use ommu\akreditasi\models\AkreditasiComponent;
?>

<div class="akreditasi-indicator-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php $component = AkreditasiComponent::getComponent();
echo $form->field($model, 'component_id')
	->dropDownList($component, ['prompt' => ''])
	->label($model->getAttributeLabel('component_id')); ?>

<?php echo $form->field($model, 'question')
	->textarea(['rows' => 4, 'cols' => 50])
	->label($model->getAttributeLabel('question')); ?>

<?php 
$choices = $model::getChoices();
$i = 0;
foreach ($choices as $key => $val) {
	$i++;
    if ($model->isNewRecord && !$model->getErrors()) {
		$model->answer = ArrayHelper::merge($model->answer, [$key => ['key' => $val]]);
    }
	$renderChoice .= $form->field($model, 'answer['.$key.']', ['template' => '{beginWrapper}'.Html::input('text', '', strtoupper($key), ['class' => 'form-control']).'{endWrapper}', 'horizontalCssClasses' => ['wrapper' => $i == 1 ? 'col-sm-2 col-xs-2 mb-4' : 'col-sm-2 col-xs-2 col-sm-offset-3 mb-4'], 'options' => ['tag' => null]])
		->textInput()
		->label($model->getAttributeLabel('answer'));
	$renderChoice .= $form->field($model, 'answer['.$key.']', ['template' => '{beginWrapper}{input}{endWrapper}', 'horizontalCssClasses' => ['wrapper' => 'col-sm-7 col-xs-10 mb-4'], 'options' => ['tag' => null]])
		->textInput()
		->label($model->getAttributeLabel('answer'));
}

echo $form->field($model, 'answer', ['template' => '{label}'.$renderChoice.'{error}{hint}', 'horizontalCssClasses' => ['error' => 'col-sm-9 col-xs-12 col-sm-offset-3', 'hint' => 'col-sm-9 col-xs-12 col-sm-offset-3']])
	->label($model->getAttributeLabel('answer')); ?>

<?php echo $form->field($model, 'order')
	->textInput(['type' => 'number'])
	->label($model->getAttributeLabel('order')); ?>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>