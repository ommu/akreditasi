<?php
/**
 * Akreditasi Indicators (akreditasi-indicator)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\IndicatorController
 * @var $model ommu\akreditasi\models\AkreditasiIndicator
 * @var $searchModel ommu\akreditasi\models\search\AkreditasiIndicator
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 23:01 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\GridView;
use yii\widgets\Pjax;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
if ($category != null) {
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['setting/admin/index']];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category'), 'url' => ['setting/category/index']];
	$this->params['breadcrumbs'][] = ['label' => $category->category_name, 'url' => ['setting/category/view', 'id' => $category->id], 'class' => 'modal-btn'];
} else {
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instrument'), 'url' => ['instrument/component/index']];
}
$this->params['breadcrumbs'][] = Yii::t('app', 'Indicators');

if ($component) {
	$this->params['menu']['content'] = [
		['label' => Yii::t('app', 'Add Indicator'), 'url' => Url::to(['create', 'id' => $component->id]), 'icon' => 'plus-square', 'htmlOptions' => ['class' => 'btn btn-success modal-btn']],
	];
}
$this->params['menu']['option'] = [
	//['label' => Yii::t('app', 'Search'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Grid Option'), 'url' => 'javascript:void(0);'],
];
?>

<div class="akreditasi-indicator-manage">
<?php Pjax::begin(); ?>

<?php if ($component != null) {
    echo $this->render('/instrument/component/admin_view', ['model' => $component, 'small' => true]);
} ?>

<?php if ($category != null) {
    echo $this->render('/setting/category/admin_view', ['model' => $category, 'small' => true]);
} ?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php echo $this->render('_option_form', ['model' => $searchModel, 'gridColumns' => $searchModel->activeDefaultColumns($columns), 'route' => $this->context->route]); ?>

<?php
$columnData = $columns;
array_push($columnData, [
	'class' => 'app\components\grid\ActionColumn',
	'header' => Yii::t('app', 'Option'),
	'urlCreator' => function($action, $model, $key, $index) {
        if ($action == 'view') {
            return Url::to(['view', 'id' => $key]);
        }
        if ($action == 'update') {
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'delete') {
            return Url::to(['delete', 'id' => $key]);
        }
	},
	'buttons' => [
		'view' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Indicator'), 'class' => 'modal-btn']);
		},
		'update' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Indicator'), 'class' => 'modal-btn']);
		},
		'delete' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				'title' => Yii::t('app', 'Delete Indicator'),
				'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'data-method'  => 'post',
			]);
		},
	],
	'template' => '{view} {update} {delete}',
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columnData,
]); ?>

<?php Pjax::end(); ?>
</div>