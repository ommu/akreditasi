<?php
/**
 * Akreditasi Components (akreditasi-component)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\AkreditasiComponent
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use app\components\widgets\ActiveForm;
use ommu\akreditasi\models\AkreditasiComponent;
use ommu\akreditasi\models\AkreditasiCategory;
?>

<div class="akreditasi-component-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php 
if ($model->isNewRecord && !$model->cat_id) {
    $category = AkreditasiCategory::getCategory();
    echo $form->field($model, 'cat_id')
        ->dropDownList($category, ['prompt' => ''])
        ->label($model->getAttributeLabel('cat_id'));
} ?>

<?php 
if ($model->parent_id) {
    $parent = AkreditasiComponent::getComponent();
    echo $form->field($model, 'parent_id')
        ->dropDownList($parent, ['prompt' => ''])
        ->label($model->getAttributeLabel('parent_id'));
} ?>

<?php echo $form->field($model, 'component_name')
	->textarea(['rows' => 3, 'cols' => 50, 'maxlength' => true])
	->label($model->getAttributeLabel('component_name')); ?>

<?php 
if (!$model->parent_id) {
    echo $form->field($model, 'bobot')
        ->textInput(['type' => 'number', 'min' => '1'])
        ->label($model->getAttributeLabel('bobot'));
} ?>

<?php echo $form->field($model, 'order')
	->textInput(['type' => 'number'])
	->label($model->getAttributeLabel('order')); ?>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>