<?php
/**
 * Akreditasi Components (akreditasi-component)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\AkreditasiComponent
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\components\grid\GridView;

if (!$small) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['setting/admin/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category'), 'url' => ['setting/category/index']];
    $this->params['breadcrumbs'][] = ['label' => $model->category->category_name, 'url' => ['setting/category/view', 'id' => $model->cat_id], 'class' => 'modal-btn'];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Component'), 'url' => ['manage', 'category' => $model->cat_id]];
    $this->params['breadcrumbs'][] = $model->component_name;

    $this->params['menu']['content'] = [
        ['label' => Yii::t('app', 'Update'), 'url' => Url::to(['update', 'id' => $model->id]), 'icon' => 'pencil', 'htmlOptions' => ['class' => 'btn btn-primary']],
        ['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
    ];
} ?>

<div class="akreditasi-component-view">

<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'publish',
		'value' => $model->quickAction(Url::to(['publish', 'id' => $model->primaryKey]), $model->publish),
		'format' => 'raw',
		'visible' => !$small,
	],
	[
		'attribute' => 'categoryName',
		'value' => function ($model) {
			$categoryName = isset($model->category) ? $model->category->category_name : '-';
            if ($categoryName != '-') {
				return Html::a($categoryName, ['setting/category/view', 'id' => $model->cat_id], ['title' => $categoryName, 'class' => 'modal-btn']);
            }
			return $categoryName;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'parentName',
		'value' => function ($model) {
			$parentName = isset($model->parent) ? $model->parent->component_name : '-';
            if ($parentName != '-') {
				return Html::a($parentName, ['instrument/component/view', 'id' => $model->parent_id], ['title' => $parentName, 'class' => 'modal-btn']);
            }
			return $parentName;
		},
		'format' => 'html',
	],
	[
		'attribute' => 'component_name',
		'value' => $model->component_name ? $model->component_name : '-',
	],
	[
		'attribute' => 'components',
		'value' => GridView::widget([
			'dataProvider' => $model->getSubComponents('dataProvider'),
			'columns' => [
				[
					'attribute' => 'component_name',
					'value' => function($model, $key, $index, $column) {
						return $model->component_name;
					},
					'enableSorting' => false,
				],
			],
			'layout' => '{items}'.Html::a(Yii::t('app', 'add sub component'), ['instrument/component/create', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'add sub component'), 'class' => 'modal-btn']).' | '.Html::a(Yii::t('app', 'show all subs'), ['instrument/component/manage', 'parent' => $model->primaryKey], ['title' => Yii::t('app', '{count} sub component', ['count' => $model->getSubComponents('count')])]),
		]),
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'bobot',
		'value' => $model->bobot ? $model->bobot : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'indicators',
		'value' => function ($model) {
			$indicators = $model->getIndicators('count');
			return Html::a($indicators, ['instrument/indicator/manage', 'component' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} indicators', ['count' => $indicators])]);
		},
		'format' => 'html',
		'visible' => !$small,
	],
	[
		'attribute' => 'order',
		'value' => $model->order ? $model->order : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'creation_date',
		'value' => Yii::$app->formatter->asDatetime($model->creation_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'creationDisplayname',
		'value' => isset($model->creation) ? $model->creation->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'modified_date',
		'value' => Yii::$app->formatter->asDatetime($model->modified_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'modifiedDisplayname',
		'value' => isset($model->modified) ? $model->modified->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'updated_date',
		'value' => Yii::$app->formatter->asDatetime($model->updated_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => '',
		'value' => Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Update'), 'class' => 'btn btn-primary btn-sm modal-btn']),
		'format' => 'html',
		'visible' => !$small && Yii::$app->request->isAjax ? true : false,
	],
];

echo DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]); ?>

</div>