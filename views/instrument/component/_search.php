<?php
/**
 * Akreditasi Components (akreditasi-component)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\search\AkreditasiComponent
 * @var $form yii\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use ommu\akreditasi\models\AkreditasiComponent;
use ommu\akreditasi\models\AkreditasiCategory;
?>

<div class="akreditasi-component-search search-form">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'options' => [
			'data-pjax' => 1
		],
	]); ?>

		<?php $parent = AkreditasiComponent::getComponent();
		echo $form->field($model, 'parent_id')
			->dropDownList($parent, ['prompt' => '']);?>

		<?php $category = AkreditasiCategory::getCategory();
		echo $form->field($model, 'cat_id')
			->dropDownList($category, ['prompt' => '']);?>

		<?php echo $form->field($model, 'component_name');?>

		<?php echo $form->field($model, 'bobot');?>

		<?php echo $form->field($model, 'creation_date')
			->input('date');?>

		<?php echo $form->field($model, 'creationDisplayname');?>

		<?php echo $form->field($model, 'modified_date')
			->input('date');?>

		<?php echo $form->field($model, 'modifiedDisplayname');?>

		<?php echo $form->field($model, 'updated_date')
			->input('date');?>

		<?php echo $form->field($model, 'publish')
			->dropDownList($model->filterYesNo(), ['prompt' => '']);?>

		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']); ?>
			<?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>