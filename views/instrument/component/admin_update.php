<?php
/**
 * Akreditasi Components (akreditasi-component)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\AkreditasiComponent
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['setting/admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category'), 'url' => ['setting/category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->category_name, 'url' => ['setting/category/view', 'id' => $model->cat_id], 'class' => 'modal-btn'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Component'), 'url' => ['manage', 'category' => $model->cat_id]];
$this->params['breadcrumbs'][] = ['label' => $model->component_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Back to Detail'), 'url' => Url::to(['view', 'id' => $model->id]), 'icon' => 'eye', 'htmlOptions' => ['class' => 'btn btn-info']],
	['label' => Yii::t('app', 'Delete'), 'url' => Url::to(['delete', 'id' => $model->id]), 'htmlOptions' => ['data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post', 'class' => 'btn btn-danger'], 'icon' => 'trash'],
];
?>

<div class="akreditasi-component-update">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>