<?php
/**
 * Akreditasi Components (akreditasi-component)
 * @var $this app\components\View
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\AkreditasiComponent
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akreditasi'), 'url' => ['admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['setting/admin/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category'), 'url' => ['setting/category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->category_name, 'url' => ['setting/category/view', 'id' => $model->cat_id], 'class' => 'modal-btn'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Component'), 'url' => ['manage', 'category' => $model->cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>

<div class="akreditasi-component-create">

<?php echo $this->render('_form', [
	'model' => $model,
]); ?>

</div>
