<?php
/**
 * AdminController
 * @var $this ommu\akreditasi\controllers\library\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiLibrary
 *
 * AdminController implements the CRUD actions for AkreditasiLibrary model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *  Import
 *  Export
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:57 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers\library;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\akreditasi\models\AkreditasiLibrary;
use ommu\akreditasi\models\search\AkreditasiLibrary as AkreditasiLibrarySearch;
use ommu\akreditasi\models\AkreditasiSimulation;
use ommu\akreditasi\models\AkreditasiCategory;
use yii\web\UploadedFile;
use thamtech\uuid\helpers\UuidHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;

class AdminController extends Controller
{
	use \ommu\traits\FileTrait;

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'suggest' => 'ommu\akreditasi\actions\LibrarySuggestAction',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all AkreditasiLibrary models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new AkreditasiLibrarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($category = Yii::$app->request->get('category')) != null) {
            $category = \ommu\akreditasi\models\AkreditasiCategory::findOne($category);
        }

		$this->view->title = Yii::t('app', 'Libraries');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'category' => $category,
		]);
	}

	/**
	 * Creates a new AkreditasiLibrary model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        $model = new AkreditasiLibrary();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi library success created.'));
                if (!$model->backToManage) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Create Library');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing AkreditasiLibrary model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi library success updated.'));
                if (!$model->backToManage) {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->subMenu = $this->module->params['library_submenu'];
		$this->view->title = Yii::t('app', 'Update Library: {library-name}', ['library-name' => $model->library_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single AkreditasiLibrary model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);

		$this->subMenu = $this->module->params['library_submenu'];
		$this->view->title = Yii::t('app', 'Detail Library: {library-name}', ['library-name' => $model->library_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing AkreditasiLibrary model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi library success deleted.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * actionPublish an existing AkreditasiLibrary model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi library success updated.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * Finds the AkreditasiLibrary model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AkreditasiLibrary the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = AkreditasiLibrary::findOne($id)) !== null) {
            return $model;
        }

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionImport()
	{
        $importFileType = 'xls, xlsx, ods, csv';
        
        if (Yii::$app->request->isPost) {
			$akreditasiPath = AkreditasiSimulation::getUploadPath();
			$importPath = join('/', [$akreditasiPath, 'import']);
			$this->createUploadDirectory($akreditasiPath, 'import');

			$errors = [];
			$importLibrary = UploadedFile::getInstanceByName('importLibrary');
            if ($importLibrary instanceof UploadedFile && !$importLibrary->getHasError()) {
                if (in_array(strtolower($importLibrary->getExtension()), $this->formatFileType($importFileType))) {
                    // category
                    $category = AkreditasiCategory::getCategory();

					$fileName = join('-', [time(), UuidHelper::uuid(), 'library']);
					$fileNameExtension = join('.', [$fileName, strtolower($importLibrary->getExtension())]);

                    if ($importLibrary->saveAs(join('/', [$importPath, $fileNameExtension]))) {
						$spreadsheet = IOFactory::load(join('/', [$importPath, $fileNameExtension]));
						$sheetData = $spreadsheet->getActiveSheet()->toArray();

						try {
							foreach ($sheetData as $key => $value) {
                                if ($key == 0) {
									continue;
                                }

								$libraryName        = trim($value[0]);
								$npp                = trim($value[1]);
								$catId              = trim($value[2]);
								$accreditation      = trim($value[3]);

								$model = new AkreditasiLibrary;
								$model->cat_id = array_keys($category, $catId)[0];
								$model->library_name = $libraryName;
								$model->npp = $npp;
								$model->accreditation = strtolower($accreditation);

                                if (!$model->save()) {
									$errors['row_'.($key+1)] = $model->getErrors();
                                }
							}

                            if (!empty($errors)) {
                                $libraryImportErrorFile = join('/', [$importPath, $fileName.'.json']);
                                if (!file_exists($libraryImportErrorFile)) {
                                    file_put_contents($libraryImportErrorFile, \yii\helpers\Json::encode($errors));
                                }
                            }

							Yii::$app->session->setFlash('success', Yii::t('app', 'Library success imported.'));
                            if (!empty($errors)) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Library success imported. (with {errors})', [
                                    'errors' => Html::a(Yii::t('app', '{warning} warning', ['warning' => count($errors)]), Url::to(join('/', ['@webpublic', AkreditasiSimulation::getUploadPath(false), 'import', $fileName.'.json'])), ['target' => '_blank']),
                                ]));
                            }

                            if (!Yii::$app->request->isAjax) {
                                return $this->redirect(['manage']);
                            }
                            return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

						} catch (\Exception $e) {
							throw $e;
						} catch (\Throwable $e) {
							throw $e;
						}
                    }

                } else {
					Yii::$app->session->setFlash('error', Yii::t('app', 'The file {name} cannot be uploaded. Only files with these extensions are allowed: {extensions}', [
						'name' => $importLibrary->name,
						'extensions' => $importFileType,
					]));
                }

            } else {
				Yii::$app->session->setFlash('error', Yii::t('app', 'Import file library cannot be blank.'));
            }

            if (!Yii::$app->request->isAjax) {
                return $this->redirect(['import']);
            }
            return $this->redirect(Yii::$app->request->referrer ?: ['import']);
		}

		$this->view->title = Yii::t('app', 'Import Library');
		$this->view->description = '';
        if (Yii::$app->request->isAjax) {
			$this->view->description = Yii::t('app', 'Are you sure you want to import library data?');
        }
		$this->view->keywords = '';
		return $this->oRender('admin_import', [
			'importFileType' => $importFileType,
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionExport()
	{
        
	}
}
