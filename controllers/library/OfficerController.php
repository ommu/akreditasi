<?php
/**
 * OfficerController
 * @var $this ommu\akreditasi\controllers\library\OfficerController
 * @var $model ommu\akreditasi\models\AkreditasiOfficer
 *
 * OfficerController implements the CRUD actions for AkreditasiOfficer model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *  Import
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers\library;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\akreditasi\models\AkreditasiOfficer;
use ommu\akreditasi\models\search\AkreditasiOfficer as AkreditasiOfficerSearch;
use ommu\akreditasi\models\AkreditasiLibrary;
use ommu\akreditasi\models\AkreditasiSimulation;
use yii\web\UploadedFile;
use thamtech\uuid\helpers\UuidHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;
use app\models\Users;

class OfficerController extends Controller
{
	use \ommu\traits\FileTrait;

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        if (Yii::$app->request->get('id') || Yii::$app->request->get('library')) {
            $this->subMenu = $this->module->params['library_submenu'];
        }
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all AkreditasiOfficer models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new AkreditasiOfficerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($library = Yii::$app->request->get('library')) != null) {
            $this->subMenuParam = $library;
            $library = \ommu\akreditasi\models\AkreditasiLibrary::findOne($library);
        }

		$this->view->title = Yii::t('app', 'Librarians');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'library' => $library,
		]);
	}

	/**
	 * Creates a new AkreditasiOfficer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        $model = new AkreditasiOfficer();
        if (($id = Yii::$app->request->get('id')) != null) {
            $model->library_id = $id;
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi librarian success created.'));
                if ($id) {
                    return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'library' => $id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'library' => $model->library_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->subMenuParam = $model->library_id;
		$this->view->title = Yii::t('app', 'Create Librarian');
        if ($id) {
            $this->view->title = Yii::t('app', 'Create Librarian: {library-name}', ['library-name' => $model->library->library_name]);
        }
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing AkreditasiOfficer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->subMenuParam = $model->library_id;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi librarian success updated.'));
                if ($model->library_id) {
                    return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'library' => $model->library_id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Update Librarian: {officer-name}', ['officer-name' => $model->user->displayname]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single AkreditasiOfficer model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);
		$this->subMenuParam = $model->library_id;

		$this->view->title = Yii::t('app', 'Detail Librarian: {officer-name}', ['officer-name' => $model->user->displayname]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing AkreditasiOfficer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi librarian success deleted.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * actionPublish an existing AkreditasiOfficer model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi librarian success updated.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * Finds the AkreditasiOfficer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AkreditasiOfficer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = AkreditasiOfficer::findOne($id)) !== null) {
            return $model;
        }

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionImport()
	{
        if (($id = Yii::$app->request->get('id')) != null) {
            $model = AkreditasiLibrary::findOne($id);
            $this->subMenuParam = $id;
        }
        $importFileType = 'xls, xlsx, ods, csv';
        
        if (Yii::$app->request->isPost) {
			$akreditasiPath = AkreditasiSimulation::getUploadPath();
			$importPath = join('/', [$akreditasiPath, 'import']);
			$this->createUploadDirectory($akreditasiPath, 'import');

			$errors = [];
			$importLibrarian = UploadedFile::getInstanceByName('importLibrarian');
            if ($importLibrarian instanceof UploadedFile && !$importLibrarian->getHasError()) {
                if (in_array(strtolower($importLibrarian->getExtension()), $this->formatFileType($importFileType))) {
					$fileName = join('-', [time(), UuidHelper::uuid(), 'librarian']);
					$fileNameExtension = join('.', [$fileName, strtolower($importLibrarian->getExtension())]);

                    if ($importLibrarian->saveAs(join('/', [$importPath, $fileNameExtension]))) {
						$spreadsheet = IOFactory::load(join('/', [$importPath, $fileNameExtension]));
						$sheetData = $spreadsheet->getActiveSheet()->toArray();

						try {
							foreach ($sheetData as $key => $value) {
                                if ($key == 0) {
									continue;
                                }

								$librarianName      = trim($value[0]);
								$email              = trim($value[1]);
								$password           = trim($value[2]);
								$librarianLevel     = trim($value[3]);

                                if ($password == '') {
                                    $password = Yii::$app->security->generateRandomString(8);
                                }

								$user = new Users;
								$user->level_id = 3;
								$user->email = $email;
								$user->displayname = $librarianName;
								$user->password = $password;;

                                if ($user->save()) {
                                    $librarian = new AkreditasiOfficer;
                                    $librarian->library_id = $id;
                                    $librarian->user_id = $user->user_id;
                                    $librarian->officer_level = $librarianLevel;

                                    if (!$librarian->save()) {
                                        $errors['row_'.($key+1).'_librarian'] = $librarian->getErrors();
                                    }

                                } else {
                                    echo '<pre>';
                                    print_r($user->getErrors());
									$errors['row_'.($key+1).'_user'] = $user->getErrors();
                                }
							}

                            if (!empty($errors)) {
                                $librarianImportErrorFile = join('/', [$importPath, $fileName.'.json']);
                                if (!file_exists($librarianImportErrorFile)) {
                                    file_put_contents($librarianImportErrorFile, \yii\helpers\Json::encode($errors));
                                }
                            }

							Yii::$app->session->setFlash('success', Yii::t('app', 'Librarian success imported.'));
                            if (!empty($errors)) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Librarian success imported. (with {errors})', [
                                    'errors' => Html::a(Yii::t('app', '{warning} warning', ['warning' => count($errors)]), Url::to(join('/', ['@webpublic', AkreditasiSimulation::getUploadPath(false), 'import', $fileName.'.json'])), ['target' => '_blank']),
                                ]));
                            }

                            if (!Yii::$app->request->isAjax) {
                                return $this->redirect(['manage', 'library' => $id]);
                            }
                            return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'library' => $id]);

						} catch (\Exception $e) {
							throw $e;
						} catch (\Throwable $e) {
							throw $e;
						}
                    }

                } else {
					Yii::$app->session->setFlash('error', Yii::t('app', 'The file {name} cannot be uploaded. Only files with these extensions are allowed: {extensions}', [
						'name' => $importLibrarian->name,
						'extensions' => $importFileType,
					]));
                }

            } else {
				Yii::$app->session->setFlash('error', Yii::t('app', 'Import file librarian cannot be blank.'));
            }

            if (!Yii::$app->request->isAjax) {
                return $this->redirect(['import']);
            }
            return $this->redirect(Yii::$app->request->referrer ?: ['import']);
		}

		$this->view->title = Yii::t('app', 'Import Librarian');
        if ($id) {
            $this->view->title = Yii::t('app', 'Import Librarian: {library-name}', ['library-name' => $model->library_name]);
        }
		$this->view->description = '';
        if (Yii::$app->request->isAjax) {
			$this->view->description = Yii::t('app', 'Are you sure you want to import library data?');
        }
		$this->view->keywords = '';
		return $this->oRender('admin_import', [
			'model' => $model,
			'importFileType' => $importFileType,
		]);
	}
}
