<?php
/**
 * IndicatorController
 * @var $this app\components\View
 *
 * Reference start
 * TOC :
 *	Go
 *	Index
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 12 November 2020, 14:48 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers\sync;

use Yii;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use ommu\akreditasi\models\AkreditasiIndicator;

class IndicatorController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function allowAction(): array {
		return [];
	}

	/**
	 * Index Action
	 */
	public function actionIndex()
	{
		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}

	/**
	 * Go Action
	 */
	public function actionGo()
	{
        $indicators = AkreditasiIndicator::find()
            ->alias('t')
            ->select(['id', 'answer']);
        // if (($category = Yii::$app->request->get('category')) != null) {

        // }
        // if (($component = Yii::$app->request->get('component')) != null) {
            
        // }
        
        $indicators = $indicators->all();
        
        if ($indicators) {
            foreach ($indicators as $model) {
                if (!is_array($model->answer['a'])) {
                    continue;
                }
                foreach ($model->answer as $key => $val) {
                    $answer[$key] = $val['val'];
                }

                $model->answer = $answer;
                $model->save(false, ['answer', 'modified_id']);
            }

            Yii::$app->session->setFlash('success', Yii::t('app', 'Indicator success sync.'));
            return $this->redirect(Yii::$app->request->referrer ?: ['instrument/indicator/manage']);
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Indicator not success sync.'));
            return $this->redirect(Yii::$app->request->referrer ?: ['instrument/indicator/manage']);
        }
	}
}
