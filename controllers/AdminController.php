<?php
/**
 * AdminController
 * @var $this ommu\akreditasi\controllers\AdminController
 * @var $model ommu\akreditasi\models\AkreditasiSimulation
 *
 * AdminController implements the CRUD actions for AkreditasiSimulation model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *	Run
 *  Complete
 *  Document
 *  Archive
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 7 October 2019, 23:58 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\akreditasi\models\AkreditasiSimulation;
use ommu\akreditasi\models\search\AkreditasiSimulation as AkreditasiSimulationSearch;
use yii\helpers\ArrayHelper;
use ommu\akreditasi\models\AkreditasiComponent;
use ommu\akreditasi\models\AkreditasiAssessment;
use yii\helpers\Inflector;
use ommu\akreditasi\models\AkreditasiSimulationHistory;

class AdminController extends Controller
{
	use \ommu\traits\FileTrait;
	use \ommu\traits\DocumentTrait;

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        if (Yii::$app->request->get('id') || Yii::$app->request->get('library')) {
            $this->subMenu = $this->module->params['library_submenu'];
        }
    }

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all AkreditasiSimulation models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new AkreditasiSimulationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($library = Yii::$app->request->get('library')) != null) {
            $this->subMenuParam = $library;
            $library = \ommu\akreditasi\models\AkreditasiLibrary::findOne($library);
        }

		$this->view->title = Yii::t('app', 'Simulations');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'library' => $library,
		]);
	}

	/**
	 * Creates a new AkreditasiSimulation model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        $model = new AkreditasiSimulation();
        if (($id = Yii::$app->request->get('id')) != null) {
            $model->library_id = $id;
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi simulation success created.'));
                if (!$model->backToManage) {
                    return $this->redirect(['run', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Create Simulation');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing AkreditasiSimulation model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->subMenuParam = $model->library_id;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi simulation success updated.'));
                if (!$model->backToManage) {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Update Simulation: {library-id}', ['library-id' => $model->library->library_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single AkreditasiSimulation model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);
		$this->subMenuParam = $model->library_id;

		$this->view->title = Yii::t('app', 'Detail Simulation: {library-id}', ['library-id' => $model->library->library_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing AkreditasiSimulation model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi simulation success deleted.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * actionPublish an existing AkreditasiSimulation model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi simulation success updated.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * Finds the AkreditasiSimulation model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AkreditasiSimulation the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = AkreditasiSimulation::findOne($id)) !== null) {
            return $model;
        }

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

	/**
	 * {@inheritdoc}
	 */
	public function actionRun($id)
	{
        $model = $this->findModel($id);

        if ($model->completed == 1) {
            return $this->redirect(['complete', 'id' => $model->id]);
        }

        if (isset($model->library)) {
            if (isset($model->library->category) && $model->library->category->getComponents(true, 1, 'null') != 0) {
                $componentParent = $model->library->category->getComponents(false, 1, 'null')->all();
                $navigationStep = $this->getNavigation($componentParent);
        
                $allStep = ArrayHelper::map($navigationStep, 'id', 'title');
                $countAllStep = $countSteps = count($allStep);
                $currentStep = array_key_first($allStep);
                if ($model->getHistories(true)) {
                    $stepHistory = $model->histories;
                    $oldStep = ArrayHelper::map($stepHistory, 'id', 'step_component_id');
                    $allStepFLip = array_flip($allStep);
                    foreach ($oldStep as $val) {
                        if (in_array($val, $allStepFLip)) {
                            unset($allStep[$val]);
                            continue;
                        }
                    }
                    $countSteps = count($allStep);
                    $currentStep = array_key_first($allStep);
                }

                $thisStep = ($countAllStep - $countSteps) + 1;
        
                $components = AkreditasiComponent::find()
                    ->andWhere(['publish' => 1])
                    ->andWhere(['cat_id' => $model->library->cat_id])
                    ->andWhere(['parent_id' => $currentStep])
                    ->all();
        
                $assessment = new AkreditasiAssessment([
                    'simulation_id' => $model->id,
                    'componentId' => $currentStep,
                ]);
                $assessment->scenario = AkreditasiAssessment::SCENARIO_SIMULATION;
        
                if (Yii::$app->request->isPost) {
                    $assessment->load(Yii::$app->request->post());
        
                    $choices = $assessment->choice;
                    if (!is_array($choices)) {
                        $assessment->scenario = AkreditasiAssessment::SCENARIO_SINGLE_CHOICE;
                    }
        
                    $result = [];
                    if ($assessment->validate()) {
                        if (is_array($choices)) {
                            foreach ($choices as $key => $choice) {
                                $condition = AkreditasiAssessment::insertAssessment($choice, $key, $assessment->simulation_id);
                                if ($condition == 0) {
                                    $result[] = Yii::t('app', '{indicator} (skip)', array('indicator' => $key));
                                } else if ($condition == 1) {
                                    $result[] = Yii::t('app', '{indicator} (success)', array('indicator' => $key));
                                } else if ($condition == 2) {
                                    $result[] = Yii::t('app', '{indicator} (error)', array('indicator' => $key));
                                }
                            }
    
                            // update step component in simulation
                            $simulation = $assessment->simulation;
                            $date = date('Y-m-d H:i:s');
                            if ($countSteps == 1) {
                                $simulation->simulation_end = $date;
                            }
                            $simulation->step_component_id = $assessment->componentId;
                            $simulation->step_component_date = $date;
                            $simulation->step_component_score = $assessment::getStepScore($choices);
                            if ($simulation->save(false, ['simulation_end', 'step_component_id', 'step_component_date', 'step_component_score'])) {
                                // update simulation score
                                if ($countSteps == 1) {
                                    $model::setScoreFinal($model->id);
                                }
    
                                Yii::$app->session->setFlash('success', Yii::t('app', 'success created.<br/>{result}', [
                                    'result' => $this->formatFileType($result, false, '<br/>'),
                                ]));
                                return $this->redirect(['run', 'id' => $simulation->id]);
                            }
        
                        } else {
                            if (AkreditasiAssessment::insertAssessment($choices, $assessment->simulation_id) == 1) {
                                Yii::$app->session->setFlash('success', Yii::t('app', 'success created.'));
                                return $this->redirect(['index']);
                            }
                        }
        
                    } else {
                        if (Yii::$app->request->isAjax) {
                            return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($assessment));
                        }
                    }
                }
    
                $data = [
                    'simulation' => true,
                    'simulationLibrary' => true,
                    'simulationLibraryCategory' => true,
                    'start' => $model->simulation_start == '0000-00-00 00:00:00' ? true : false,
                    'navigation' => $navigationStep,
                    'current' => $currentStep,
                    'step' => $thisStep,
                    'components' => $components,
                    'assessment' => $assessment,
                ];

                $this->view->title = Yii::t('app', 'Simulation: Component {step}', [
                    'step' => $allStep[$currentStep],
                ]);

            } else {
                $data = [
                    'simulation' => false,
                    'simulationLibrary' => true,
                    'simulationLibraryCategory' => false,
                ];
                $this->view->title = Yii::t('app', 'Simulation Start');
            }

            $this->view->cards = false;

        } else {
            $data = [
                'simulation' => false,
                'simulationLibrary' => false,
            ];
            $this->view->title = Yii::t('app', 'Simulation Start');

        }

        $this->subMenu = null;
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_run', ArrayHelper::merge([
			'model' => $model,
        ], $data));
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionComplete($id)
	{
        $model = $this->findModel($id);

        if ($model->completed != 1) {
            return $this->redirect(['run', 'id' => $model->id]);
        }

        $this->subMenu = null;
        $this->view->title = Yii::t('app', 'Simulation Complete');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_complete', [
			'model' => $model,
        ]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionDocument($id)
	{
        $model = $this->findModel($id);

        if (!$model->completed) {
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Cannot generated document. Your simulation is not completed.'));
            return $this->redirect(['manage', 'library' => $model->library_id]);
        }

        $model->scenario = $model::SCENARIO_DOCUMENT;

        $librarian = [
            'headDepartment' => [],
            'headLibrary' => [],
            'librarian' => [],
        ];

        if ($model->library->officer) {
            $librarian['headDepartment'] = ArrayHelper::map($model->library->getOfficers(false, 1, 'headDepartment')->all(), 'user.displayname', 'user.displayname');
            $librarian['headLibrary'] = ArrayHelper::map($model->library->getOfficers(false, 1, 'headLibrary')->all(), 'user.displayname', 'user.displayname');
            $librarian['librarian'] = ArrayHelper::map($model->library->getOfficers(false, 1, 'librarian')->all(), 'user.displayname', 'user.displayname');
        }

        if (!$model->getErrors()) {
			$simulationDocument = $model->simulation_document;
            if (!is_array($simulationDocument)) {
				$simulationDocument = [];
            }
		}

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
				ini_set('max_execution_time', 0);
				ob_start();

                if (!$model->document || ($model->document && $model->regenerate)) {
					$documents = [];

					$akreditasiPath = $model::getUploadPath();
					$documentPath = join('/', [$akreditasiPath, 'document']);
					$libraryPath = join('/', [$documentPath, $model->library_id]);
					$verwijderenPath = join('/', [$akreditasiPath, 'verwijderen']);
                    $model->createUploadDirectory($akreditasiPath, 'document');
                    $model->createUploadDirectory($documentPath, $model->library_id);

                    // profile
                    $profileName = self::getProfileDoc($model);
                    // array_push($documents, $profileName);
                    $documents = ArrayHelper::merge($documents, ['profile' => $profileName]);

                    // recap
                    $recapName = self::getRecapDoc($model);
                    // array_push($documents, $recapName);
                    $documents = ArrayHelper::merge($documents, ['recap' => $recapName]);

                    // assessment
                    $assessmentName = self::getAssessmentDoc($model);
                    // array_push($documents, $assessmentName);
                    $documents = ArrayHelper::merge($documents, ['assessment' => $assessmentName]);

                    $model->simulation_document = $documents;

                    if ($model->save()) {
                        if (!empty($simulationDocument)) {
							foreach ($simulationDocument as $key => $val) {
                                if (file_exists(join('/', [$libraryPath, $val]))) {
									rename(join('/', [$libraryPath, $val]), join('/', [$verwijderenPath, $model->library_id.'-'.time().'_regenerate_'.$val]));
                                }
							}
						}
					}
                }

				Yii::$app->session->setFlash('success', Yii::t('app', 'Simulation success generated document.'));
                return $this->redirect(['document', 'id' => $model->id]);
	
				ob_end_flush();

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }
        
		$this->subMenuParam = $model->library_id;
		$this->view->title = Yii::t('app', 'Document Simulation: {library-id}', ['library-id' => $model->library->library_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_document', [
			'model' => $model,
			'librarian' => $librarian,
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionArchive($id)
	{
        $model = $this->findModel($id);

        if (!$model->completed) {
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Cannot generated document. Your simulation is not completed.'));
            return $this->redirect(['manage', 'library' => $model->library_id]);
        }

        if (!$model->document) {
            Yii::$app->session->setFlash('warning', Yii::t('app', 'Cannot download zip document. Your simulation document not been created .'));
            return $this->redirect(['document', 'id' => $model->id]);
        }

        //zip
        if (array_key_exists('archive', $model->simulation_document)) {
            $archiveName = $model->simulation_document['archive'];

        } else {
            $simulationDocument = array_values($model::getDocumentUrl($model->simulation_document, $model->library_id, false));
            $archiveName = join('-', [time(), $model->library_id, $model->id]).'.zip';
            Yii::$app->zipper->create(join('/', [$model::getUploadPath(), 'document', $model->library_id, $archiveName]), $simulationDocument, true, 'zip');

            $model->simulation_document = ArrayHelper::merge($model->simulation_document, ['archive' => $archiveName]);
    
            $model->save();
        }

        return $this->redirect(Url::to(join('/', ['@webpublic', $model::getUploadPath(false), 'document', $model->library_id, $archiveName])));
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionPreview($id)
	{
        $model = $this->findModel($id);

        // profile
        // self::getProfileDoc($model, true);
        // recap
        // self::getRecapDoc($model, true);
        // assessment
        self::getAssessmentDoc($model, true);
    }

	/**
	 * {@inheritdoc}
	 */
	public static function getLibraryPath($model)
	{
        $akreditasiPath = $model::getUploadPath();
        $documentPath = join('/', [$akreditasiPath, 'document']);
        $libraryPath = join('/', [$documentPath, $model->library_id]);
        $verwijderenPath = join('/', [$akreditasiPath, 'verwijderen']);
        $model->createUploadDirectory($akreditasiPath, 'document');
        $model->createUploadDirectory($documentPath, $model->library_id);

        return $libraryPath;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getProfileDoc($model, $preview=false)
	{
        $libraryPath = self::getLibraryPath($model);

        $templatePath = Yii::getAlias('@ommu/akreditasi/components/templates');
        $letterTemplate = join('/', [$templatePath, 'document_profile.php']);
        $letterName = Inflector::slug(join('-', [time(), $model->library_id, $model->id, 'profile']));

        // headDepartment
        $headDepartment = '';
        if (in_array($model->library->cat_id, [5, 6, 7])) {             // Sekolah
            $headDepartment = Yii::t('app', 'Kepala Sekolah');
        } else if ($model->library->cat_id == 4) {                      // Desa/Kelurahan
            $headDepartment = Yii::t('app', 'Kepala Desa/Kelurahan');
        } else if ($model->library->cat_id == 3) {                      // Kecamatan
            $headDepartment = Yii::t('app', 'Camat');
        } else if ($model->library->cat_id == 8) {                      // Perguruan Tinggi
            $headDepartment = Yii::t('app', 'Kepala Perguruan Tinggi');
        }

        // headLibrary
        $headLibrary = 'Kepala Perpustakaan';
        if (in_array($model->library->cat_id, [1, 2])) {                      // Perguruan Tinggi
            $headLibrary = Yii::t('app', 'Kepala Dinas Perpustakaan');
        }

        // address
        $address = $model->library->library_address;
        $city = \ommu\core\models\CoreZoneCity::getInfo($address['city'], 'city_name');
        $province = \ommu\core\models\CoreZoneProvince::getInfo($address['province'], 'province_name');
        $generatedDateHead = Yii::$app->formatter->asDate($model->generated_date, 'full');
        $generatedDateSignature = Yii::$app->formatter->asDate($model->generated_date, 'long');
        // contact
        $contact = $model->library->library_contact;
        $phoneFax = [];
        if ($contact['phone'] != '') {
            array_push($phoneFax, 'Telp: '.$contact['phone']);
        }
        if ($contact['fax'] != '') {
            array_push($phoneFax, 'Fax: '.$contact['fax']);
        }
        if ($contact['hotline'] != '') {
            $hotline = $model->formatFileType(preg_split('/\n|\r\n?\s*/', $contact['hotline']), false, ', ');
            array_push($phoneFax, 'HP: '.$hotline);
        }

        $fileName = $this->getPdf([
            'model' => $model,
            'headDepartment' => $headDepartment,
            'headLibrary' => $headLibrary,
            'generatedDateHead' => $generatedDateHead,
            'generatedDateSignature' => $generatedDateSignature,
            // address
            'place' => $address['place'],
            'village' => $address['village'],
            'district' => $address['district'],
            'city' => $city,
            'province' => $province,
            // contact
            'phoneFax' => implode('<br/>', $phoneFax),
            'email' => $contact['email'],
            'librarian' => Html::ol(explode(',', $model->librarian['librarian']), ['encode' => false]),
        ], $letterTemplate, $libraryPath, $letterName, $preview);

        return $fileName;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getRecapDoc($model, $preview=false)
	{
        $libraryPath = self::getLibraryPath($model);

        $templatePath = Yii::getAlias('@ommu/akreditasi/components/templates');
        $letterTemplate = join('/', [$templatePath, 'document_recap.php']);
        $letterName = Inflector::slug(join('-', [time(), $model->library_id, $model->id, 'recap']));

        $fileName = $this->getPdf([
            'model' => $model,
        ], $letterTemplate, $libraryPath, $letterName, $preview);

        return $fileName;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getAssessmentDoc($model, $preview=false)
	{
        $libraryPath = self::getLibraryPath($model);

        $templatePath = Yii::getAlias('@ommu/akreditasi/components/templates');
        $letterTemplate = join('/', [$templatePath, 'document_assessment.php']);
        $letterName = Inflector::slug(join('-', [time(), $model->library_id, $model->id, 'assessment']));

        $components = $model->library->category->getComponents(false, 1, 'null')->all();
        $assessment = self::getAssessment($components, $model->id);

        $fileName = $this->getPdf([
            'model' => $model,
            'assessmentHtml' => self::parseAssessment($assessment),
        ], $letterTemplate, $libraryPath, $letterName, $preview);

        return $fileName;
	}

	/**
	 * {@inheritdoc}
	 */
	public function allowAction(): array {
		return ['preview'];
	}
    
	/**
	 * {@inheritdoc}
	 */
	public function getNavigation($components)
	{
        $data = [];
        if (is_array($components) && !empty($components)) {
            foreach ($components as $key => $val) {
                $data[$key] = [
                    'id' => $val->id,
                    'title' => $val->component_name,
                    'icon' => '',
                    'desc' => Yii::t('app', 'Bobot {bobot}% ({indicator} indicator)', [
                        'bobot' => $val->bobot,
                        'indicator' => $val->getIndicators('count', 1, true),
                    ]),
                ];
            }
        }

        return $data;
    }

	/**
	 * {@inheritdoc}
	 */
    public static function getAssessment($components, $simulationId, $i=0)
    {
        if (!empty($components)) {
            $j;
            foreach ($components as $key => $component) {
                $j++;
                $label = $i == 0 ? $j : join('.', [$i, $j]);
                $data = [
                    'id' => $component->id,
                    'label' => $label,
                    'componentName' => $component->component_name,
                ];

                // indicator
                if ($component->getIndicators('count') != 0) {
                    $indicatorItem = [];
                    $k = 0;
                    foreach ($component->indicators as $key => $indicator) {
                        $k++;
                        $choice = AkreditasiAssessment::getChoice($simulationId, $indicator->id);
                        $indicatorItem[] = [
                            'id' => $indicator->id,
                            'label' => join('.', [$label, $k]),
                            'question' => $indicator->question,
                            'choice' => strtoupper($choice),
                            'score' => AkreditasiAssessment::getChoiceScore($choice),
                        ];
                    }
                    $data = ArrayHelper::merge($data, [
                        'indicator' => $indicatorItem,
                    ]);
                }

                // sub component
                if ($component->getSubComponents('count') != 0) {
                    $data = ArrayHelper::merge($data, [
                        'component' => self::getAssessment($component->subComponents, $simulationId, $label),
                    ]);
                }

                // total
                if ($i == 0) {
                    $data = ArrayHelper::merge($data, [
                        'total' => [
                            'componentName' => join(' ', ['Jumlah Skor', $component->component_name]),
                            'score' => AkreditasiSimulationHistory::getStepScore($simulationId, $component->id),
                        ],
                    ]);
                }

                $assessment[] = $data;
            }
        }

        return $assessment;
    }

	/**
	 * {@inheritdoc}
	 */
    public static function parseAssessment($assessment)
    {
        if (!empty($assessment)) {
            $html = '
            <table class="profile" style="width: 100%;">
                <tr>
                    <th style="vertical-align: middle; text-align: center;">No</th>
                    <th style="vertical-align: middle; text-align: center;">Komponen</th>
                    <th style="vertical-align: middle; text-align: center;">Skor<br/>Huruf</th>
                    <th style="vertical-align: middle; text-align: center;">Skor<br/>Angka</th>
                </tr>';

            foreach ($assessment as $val) {
                $html .= self::parseComponent($val);
            }

            $html .= '</table>';
        
            return $html;
        }

        return '';
    }

	/**
	 * {@inheritdoc}
	 */
    public static function parseComponent($data)
    {
        $html = '
        <tr>
            <td style="vertical-align: top;">'.$data['label'].'</td>
            <td style="vertical-align: top;">'.$data['componentName'].'</td>
            <td></td>
            <td></td>
        </tr>';

        // indicator
        if (array_key_exists('indicator', $data)) {
            foreach ($data['indicator'] as $indicator) {
                $html .= '
                <tr>
                    <td style="vertical-align: top;">'.$indicator['label'].'</td>
                    <td style="vertical-align: top; width: 71%">'.$indicator['question'].'</td>
                    <td style="vertical-align: top; text-align: center;">'.$indicator['choice'].'</td>
                    <td style="vertical-align: top; text-align: center;">'.$indicator['score'].'</td>
                </tr>';
            }
        }

        // sub component
        if (array_key_exists('component', $data)) {
            foreach ($data['component'] as $subComponent) {
                $html .= self::parseComponent($subComponent);
            }
        }

        // total
        if (array_key_exists('total', $data)) {
            $total = $data['total'];
            $html .= '
            <tr>
                <th style="vertical-align: top;"></th>
                <th style="vertical-align: top; width: 71%">'.$total['componentName'].'</th>
                <th style="vertical-align: top;"></th>
                <th style="vertical-align: top; text-align: center;">'.$total['score'].'</th>
            </tr>';
        }

        return $html;
    }
}
