<?php
/**
 * IndicatorController
 * @var $this ommu\akreditasi\controllers\instrument\IndicatorController
 * @var $model ommu\akreditasi\models\AkreditasiIndicator
 *
 * IndicatorController implements the CRUD actions for AkreditasiIndicator model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 23:01 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers\instrument;

use Yii;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\akreditasi\models\AkreditasiIndicator;
use ommu\akreditasi\models\search\AkreditasiIndicator as AkreditasiIndicatorSearch;

class IndicatorController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        if (Yii::$app->request->get('id') || Yii::$app->request->get('component') || Yii::$app->request->get('category')) {
            $this->subMenu = $this->module->params['category_submenu'];
        }
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all AkreditasiIndicator models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new AkreditasiIndicatorSearch();
        if (($category = Yii::$app->request->get('category')) != null) {
            $searchModel = new AkreditasiIndicatorSearch(['categoryId' => $category]);
        }
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($component = Yii::$app->request->get('component')) != null) {
            $component = \ommu\akreditasi\models\AkreditasiComponent::findOne($component);
			$this->subMenuParam = $component->cat_id;
		}
        if ($category) {
			$this->subMenuParam = $category;
			$category = \ommu\akreditasi\models\AkreditasiCategory::findOne($category);
		}

		$this->view->title = Yii::t('app', 'Indicators');
        if ($category) {
            $this->view->title = Yii::t('app', 'Indicators: Category Akreditasi {category-name}', ['category-name' => $category->category_name]);
        }
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'component' => $component,
			'category' => $category,
		]);
	}

	/**
	 * Creates a new AkreditasiIndicator model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        if (($id = Yii::$app->request->get('id')) == null) {
            throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

		$model = new AkreditasiIndicator();
        if ($id) {
            $model->component_id = $id;
        }

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi indicator success created.'));
                if ($id) {
                    return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'component' => $id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'component' => $model->component_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->subMenuParam = $model->component->cat_id;
		$this->view->title = Yii::t('app', 'Create Indicator');
        if ($id) {
            $this->view->title = Yii::t('app', 'Create Indicator: {component-name}', ['component-name' => $model->component->component_name]);
        }
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing AkreditasiIndicator model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->subMenuParam = $model->component->cat_id;

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi indicator success updated.'));
                if (!Yii::$app->request->isAjax) {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'component' => $model->component_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Update Indicator: {component-name}', ['component-name' => $model->component->component_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single AkreditasiIndicator model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);
		$this->subMenuParam = $model->component->cat_id;

		$this->view->title = Yii::t('app', 'Detail Indicator: {component-name}', ['component-name' => $model->component->component_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing AkreditasiIndicator model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi indicator success deleted.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'component' => $model->component_id]);
		}
	}

	/**
	 * actionPublish an existing AkreditasiIndicator model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi indicator success updated.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'component' => $model->component_id]);
		}
	}

	/**
	 * Finds the AkreditasiIndicator model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AkreditasiIndicator the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = AkreditasiIndicator::findOne($id)) !== null) {
            return $model;
        }

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}
}
