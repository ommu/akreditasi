<?php
/**
 * ComponentController
 * @var $this ommu\akreditasi\controllers\instrument\ComponentController
 * @var $model ommu\akreditasi\models\AkreditasiComponent
 *
 * ComponentController implements the CRUD actions for AkreditasiComponent model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 14 August 2019, 18:26 WIB
 * @link https://bitbucket.org/ommu/akreditasi
 *
 */

namespace ommu\akreditasi\controllers\instrument;

use Yii;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\akreditasi\models\AkreditasiComponent;
use ommu\akreditasi\models\search\AkreditasiComponent as AkreditasiComponentSearch;

class ComponentController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        if (Yii::$app->request->get('id') || Yii::$app->request->get('category') || Yii::$app->request->get('parent')) {
            $this->subMenu = $this->module->params['category_submenu'];
        }
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all AkreditasiComponent models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new AkreditasiComponentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($category = Yii::$app->request->get('category')) != null) {
            $this->subMenuParam = $category;
			$category = \ommu\akreditasi\models\AkreditasiCategory::findOne($category);
		}
        if (($parent = Yii::$app->request->get('parent')) != null) {
            $parent = \ommu\akreditasi\models\AkreditasiComponent::findOne($parent);
			$this->subMenuParam = $parent->cat_id;
		}

		$this->view->title = Yii::t('app', 'Components');
        if ($category) {
            $this->view->title = Yii::t('app', 'Components Category: Akreditasi {category-name}', ['category-name' => $category->category_name]);
        }
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'category' => $category,
			'parent' => $parent,
		]);
	}

	/**
	 * Creates a new AkreditasiComponent model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        if (($category = Yii::$app->request->get('category')) == null && ($id = Yii::$app->request->get('id')) == null) {
            throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

		$model = new AkreditasiComponent();
        if ($category) {
            $model->cat_id = $category;
        }
        if ($id) {
			$model->parent_id = $id;
			$model->cat_id = $model->parent->cat_id;
		}

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi component success created.'));
                if ($category) {
                    return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'category' => $category]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'category' => $model->cat_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->subMenuParam = $model->cat_id;
		$this->view->title = Yii::t('app', 'Create Component');
        if ($category) {
            $this->view->title = Yii::t('app', 'Create Component: Akreditasi {category-name}', ['category-name' => $model->category->category_name]);
        }
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing AkreditasiComponent model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->subMenuParam = $model->cat_id;

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi component success updated.'));
                if (!Yii::$app->request->isAjax) {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'category' => $model->cat_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Update Component: {component-name}', ['component-name' => $model->component_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single AkreditasiComponent model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);
		$this->subMenuParam = $model->cat_id;

		$this->view->title = Yii::t('app', 'Detail Component: {component-name}', ['component-name' => $model->component_name]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing AkreditasiComponent model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi component success deleted.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'category' => $model->cat_id]);
		}
	}

	/**
	 * actionPublish an existing AkreditasiComponent model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'Akreditasi component success updated.'));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage', 'category' => $model->cat_id]);
		}
	}

	/**
	 * Finds the AkreditasiComponent model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AkreditasiComponent the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = AkreditasiComponent::findOne($id)) !== null) {
            return $model;
        }

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}
}
